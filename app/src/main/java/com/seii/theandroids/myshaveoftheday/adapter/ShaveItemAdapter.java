package com.seii.theandroids.myshaveoftheday.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.seii.theandroids.myshaveoftheday.R;
import com.seii.theandroids.myshaveoftheday.model.ConsumableShaveItem;
import com.seii.theandroids.myshaveoftheday.model.DisposableShaveItem;
import com.seii.theandroids.myshaveoftheday.model.ITEM_TYPE;
import com.seii.theandroids.myshaveoftheday.model.ShaveItem;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 */
public class ShaveItemAdapter extends ArrayAdapter<ShaveItem>
{
    private List<ShaveItem> shaveItems;

    public ShaveItemAdapter(Context context, int resource, List<ShaveItem> items) {
        super(context, resource, items);
        shaveItems = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View view = convertView;

        if (view == null)
        {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            view = vi.inflate(R.layout.shave_item_list_row, null);
        }

        ShaveItem item = getItem(position);

        if (item != null)
        {
            TextView title = (TextView)view.findViewById(R.id.itemTitleText),
                        type = (TextView)view.findViewById(R.id.itemTypeText),
                        special = (TextView)view.findViewById(R.id.itemSpecialText),
                        lastUse = (TextView)view.findViewById(R.id.itemLastUseText);

            title.setText(item.getTitle());
            if(item.getType() == ITEM_TYPE.REGULAR)
            {
                type.setText("Regular");
                special.setText("");
            }
            else if(item.getType() == ITEM_TYPE.DISPOSABLE)
            {
                type.setText("Disposable");
                special.setText("Units left: " + ((DisposableShaveItem)item).getUnitsLeft());
            }
            else if(item.getType() == ITEM_TYPE.CONSUMABLE)
            {
                type.setText("Consumable");
                special.setText("Uses: " + ((ConsumableShaveItem)item).getNumberOfUses());
            }

            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            lastUse.setText("Last Use: " + sdf.format(item.getLastUse().getTime()));

        }

        return view;
    }


    public ShaveItem getShaveItemAt(int position)
    {
        return shaveItems.get(position);
    }
}
