package com.seii.theandroids.myshaveoftheday.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Clay on 1/25/2016.
 *
 * This class provides a higher level frame for containing groups of Shave Items from the shaveItem class.
 */
public class ItemCategory
{
    private String title;
    private List<ShaveItem> items;

    public ItemCategory()
    {
        items = new ArrayList<ShaveItem>(); // Arraylist has to be initialized before attempting to use it or add to it

    }
    public ItemCategory(String title)
    {
        this.title = title;
        items = new ArrayList<ShaveItem>();
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void addItem(ShaveItem item)
    {
        items.add(item);
    }

    public List<ShaveItem> getShaveItems()
    {
        return this.items;
    }

    /**
     * Merges the selected itemCategory with another one. The name changes to a combination of both categories with an "and" in the middle.
     * @param category The category to be merged into the selected one
     */
    public void mergeWith(ItemCategory category)
    {
        title = title + " and " + category.title;
        items.addAll(category.items);
    }

    /**
     * searches ArrayList for specified item name and deletes named items
     * from ArrayList.
     * @param name
     */
    public void deleteItem(String name)
    {
        for(int x =0; x < items.size(); x++)
        {
            if(name.equalsIgnoreCase(items.get(x).getTitle()))
                items.remove(x);
        }
    }
}

