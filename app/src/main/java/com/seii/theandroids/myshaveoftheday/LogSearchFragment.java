package com.seii.theandroids.myshaveoftheday;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.seii.theandroids.myshaveoftheday.adapter.LogAdapter;
import com.seii.theandroids.myshaveoftheday.listener.OnBackPressedListener;
import com.seii.theandroids.myshaveoftheday.model.LogEntry;
import com.seii.theandroids.myshaveoftheday.util.animationUtil;

import java.util.ArrayList;

/**
 * LogSearchFragment is responsible for populating a ListView of LogEntries with the results from a search. The user is able to click on a search result to expand the LogEntry.
 */
public class LogSearchFragment extends Fragment
{

    public LogSearchFragment()
    {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_log, container, false);
        setHasOptionsMenu(true);

        // Hides tabs and FAB and sets title
        final MainActivity mainActivity = (MainActivity)getActivity();
        animationUtil.hideTabs(mainActivity);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.search_results);
        mainActivity.fab.setVisibility(View.INVISIBLE);

        // Retrieves search results and populates ListView
        String s = getArguments().getString("SEARCH");
        DBHandler db = new DBHandler(getActivity(),null,null,1);
        ArrayList<LogEntry> results = db.searchEntries(s);
        ListView lv = (ListView) view.findViewById(R.id.logListView);
        lv.setAdapter(new LogAdapter(getActivity(), results));

        // Back press listener
        OnBackPressedListener listener = new OnBackPressedListener()
        {
            @Override
            public void doBack()
            {
                animationUtil.showTabs(getActivity());
                ((MainActivity) getActivity()).setOnBackPressedListener(null);
                ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.SOTD);

                final MainActivity mainActivity = (MainActivity)getActivity();
                mainActivity.fab.setVisibility(View.VISIBLE);
                mainActivity.fab.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        animationUtil.hideTabs(mainActivity);
                        Fragment newFragment = new AddEntryFragment();
                        FragmentTransaction transaction = mainActivity.getSupportFragmentManager().beginTransaction();
                        mainActivity.getSupportActionBar().setTitle("Add new entry");
                        mainActivity.fab.setVisibility(View.INVISIBLE);
                        Bundle args = new Bundle();
                        args.putInt("FUNCTION", 1);
                        newFragment.setArguments(args);
                        transaction.replace(R.id.root_frame, newFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                });
            }
        };
        ((MainActivity) getActivity()).setOnBackPressedListener(listener);


        // sets listener on ListView
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {

                // Opens a new fragment with a more detailed view of the entry
                Fragment newFragment = new LogEntryFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                Bundle args = new Bundle();

                // Sends position to LogEntryFragment
                args.putInt("POSITION", position);

                newFragment.setArguments(args);
                transaction.replace(R.id.root_frame, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
