package com.seii.theandroids.myshaveoftheday;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.seii.theandroids.myshaveoftheday.adapter.LogAdapter;
import com.seii.theandroids.myshaveoftheday.model.LogEntry;
import com.seii.theandroids.myshaveoftheday.util.animationUtil;

import java.util.ArrayList;

/**
 * LogFragment is responsible for providing the user with a scrollable ListView of all of their "Shave of the Day" LogEntries
 * This is the primary view for the "Shave of the Day" function of the app
 * Provides users with options to expand a LogEntry, add new LogEntry, or search LogEntries
 */
public class LogFragment extends Fragment {

    private ArrayList<LogEntry> entries;
    private DBHandler db;
    private ListView lv;

    private static final int EDIT_ENTRY = 2;

    // This keeps track of whether or not the mainActivity UI should change
    // This makes sure that when the app first opens, the shave den mainActivity UI is setup, not this one
    private boolean shouldInterfaceChange = true;


    public LogFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_log, container, false);
        setHasOptionsMenu(true);

        animationUtil.showTabs(getActivity());

        if(shouldInterfaceChange)
        {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.SOTD);

            MainActivity mainActivity = (MainActivity)getActivity();
            if (mainActivity.fab.getVisibility() == View.INVISIBLE)
                mainActivity.fab.setVisibility(View.VISIBLE);
            mainActivity.fab.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    animationUtil.hideTabs(getActivity());
                    Fragment newFragment = new AddEntryFragment();
                    Bundle args = new Bundle();
                    // Tells AddEntryFragment that it is editing an entry
                    args.putInt("FUNCTION", 1);
                    newFragment.setArguments(args);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.root_frame, newFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            });
        }
        else // Since we only need the code ignore once upon startup, it should always run afterwards.
            shouldInterfaceChange = true;


        // Populates ListView with Log Entries
        lv = (ListView) view.findViewById(R.id.logListView);
        db = new DBHandler(getActivity(), null, null, 1);
        entries = db.getLogEntries();
        lv.setAdapter(new LogAdapter(getActivity(), entries));

        // sets listener on ListView
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // Opens a new LogEntryFragment with a more detailed view of the entry
                Fragment newFragment = new LogEntryFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                Bundle args = new Bundle();

                // Sends position to LogEntryFragment
                args.putInt("POSITION", position);

                newFragment.setArguments(args);
                transaction.replace(R.id.root_frame, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        // On press and hold, displays dialog with options to edit, delete, and share
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                final int position = pos;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Choose")
                        .setItems(R.array.onLongPressSOTD, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == 0) // Edit
                                {
                                    // When clicked, opens a new fragment with a more detailed view of the entry
                                    Fragment newFragment = new AddEntryFragment();
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                    Bundle args = new Bundle();

                                    // Tells AddEntryFragment that it is editing an entry
                                    args.putInt("FUNCTION", EDIT_ENTRY);

                                    //Sends to data to newFragment in args
                                    args.putInt("POSITION", position);

                                    newFragment.setArguments(args);
                                    transaction.replace(R.id.root_frame, newFragment);
                                    transaction.addToBackStack(null);
                                    transaction.commit();
                                }
                                else if (which == 1) // Delete
                                {
                                    LogEntry entryToDel = entries.get(position);
                                    db.deleteLogEntry(entryToDel.getDescription(), entryToDel.getImage(), entryToDel.getComment());
                                }
                                else if(which == 2) // Share
                                {
                                    entries.get(position).share(getActivity());
                                }

                                // Refreshes list of Log Entries
                                final ArrayList<LogEntry> entries = db.getLogEntries();
                                lv.setAdapter(new LogAdapter(getActivity(), entries));
                            }
                        });
                Dialog d = builder.create();
                d.show();

                return true;
            }
        });

        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    public void setShouldInterfaceChange(boolean shouldInterfaceChange)
    {
        this.shouldInterfaceChange = shouldInterfaceChange;
    }
}
