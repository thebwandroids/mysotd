package com.seii.theandroids.myshaveoftheday.listener;

/**
 * Listener interface for when the user backs out of a fragment.
 */
public interface OnBackPressedListener {
    public void doBack();
}
