package com.seii.theandroids.myshaveoftheday.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.seii.theandroids.myshaveoftheday.R;
import com.seii.theandroids.myshaveoftheday.model.LogEntry;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Adapter for populating ListView with LogEntries
 */

public class LogAdapter extends ArrayAdapter<LogEntry> {


    public LogAdapter(Context context, ArrayList<LogEntry> entries) {
        super(context, R.layout.log_row_layout, entries);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder;
        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(R.layout.log_row_layout, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.desc = (TextView) view.findViewById(R.id.descTextView);
            viewHolder.img = (ImageView) view.findViewById(R.id.logImageView);
            viewHolder.date = (TextView) view.findViewById(R.id.dateTextView);
            view.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        LogEntry logEntry = getItem(position);
        if (logEntry != null) {
            viewHolder.desc.setText(logEntry.getDescription()); //Sets description
            setPhoto(logEntry.getImage(), viewHolder.img); //Sets image

            //Sets date
            Calendar c = logEntry.getDate();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
            viewHolder.date.setText(sdf.format(c.getTime()));
        }

        return view;
    }

    /**
     * Generates a bitmap from file of mCurrentPhotoPath and sets it to the preview photo ImageView
     * @param path image path
     * @param iv ImageView
     */
    private void setPhoto(String path, ImageView iv)
    {
        if (!path.equals("") && new File(path).exists()) {

            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inSampleSize = 4;

            Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
            iv.setImageBitmap(bitmap);
        }
    }

    static class ViewHolder {
        ImageView img;
        TextView desc;
        TextView date;
    }

}
