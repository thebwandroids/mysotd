package com.seii.theandroids.myshaveoftheday.model;

import java.util.Calendar;

/**
 *
 */
public interface ShaveItem
{
    // Dynamic functionality
    int getMenu();
    ITEM_TYPE getType();


    // Everything below this is standard shave item getters and setters
    String getTitle();
    String getImage();
    Calendar getPurchaseDate();
    Calendar getLastUse();
    double getPurchasePrice();
    String getNote();

    void setTitle(String title);
    void setImage(String path);
    void setPurchaseDate(Calendar date);
    void setLastUse(Calendar date);
    void setPurchasePrice(double price);
    void setNote(String note);

    void useItem();

}
