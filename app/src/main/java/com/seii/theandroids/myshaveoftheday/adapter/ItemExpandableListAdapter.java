package com.seii.theandroids.myshaveoftheday.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.seii.theandroids.myshaveoftheday.R;
import com.seii.theandroids.myshaveoftheday.model.ItemCategory;
import com.seii.theandroids.myshaveoftheday.model.ShaveItem;

import java.util.HashMap;
import java.util.List;

/**
 * Adapter for populating an ExpandableListView with Categories and ShaveItems
 */
public class ItemExpandableListAdapter extends BaseExpandableListAdapter
{
    private Context context;
    private List<ItemCategory> categories;
    private HashMap<ItemCategory, List<ShaveItem>> mapItems;

    public ItemExpandableListAdapter(Context c, List<ItemCategory> list, HashMap<ItemCategory, List<ShaveItem>> map)
    {
        this.context = c;
        this.categories = list;
        this.mapItems = map;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = getGroup(groupPosition).getTitle();
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.category_list_row_layout, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.categoryText);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = getChild(groupPosition, childPosition).getTitle();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_row_layout, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.itemText);

        txtListChild.setText(childText);
        return convertView;
    }


    @Override
    public int getGroupCount() {
        return categories.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mapItems.get(categories.get(groupPosition)).size();
    }

    @Override
    public ItemCategory getGroup(int groupPosition) {
        return categories.get(groupPosition);
    }

    @Override
    public ShaveItem getChild(int groupPosition, int childPosition) {
        return mapItems.get(categories.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
