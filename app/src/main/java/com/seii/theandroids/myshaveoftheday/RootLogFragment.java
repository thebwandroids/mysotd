package com.seii.theandroids.myshaveoftheday;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.seii.theandroids.myshaveoftheday.util.animationUtil;

/**
 * Created by Russell on 2/24/2016.
 */
public class RootLogFragment extends Fragment
{

    SearchView searchView;
    View view;

    private boolean shouldInterfaceChange = true;

    public RootLogFragment()
    {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        /*
        MenuItem item = menu.add("Search");
        item.setIcon(android.R.drawable.ic_menu_search);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        searchView = new SearchView(getActivity());
        setSearch();
        item.setActionView(searchView);
        */
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        setSearch();
    }

    private void setSearch() {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String s = searchView.getQuery().toString();


                Fragment newFragment = new LogSearchFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                Bundle args = new Bundle();

                //Sends to data to newFragment in args
                args.putString("SEARCH", s);

                newFragment.setArguments(args);
                transaction.replace(R.id.root_frame, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
//              if (searchView.isExpanded() && TextUtils.isEmpty(newText)) {
                callSearch(newText);
//              }
                return true;
            }

            public void callSearch(String query) {
                //Do searching
            }

        });


    }

    @Override
    public void onPrepareOptionsMenu(Menu menu)
    {
        if(menu != null)
        {
            MenuItem item = menu.findItem(R.id.action_settings);
            if (item != null)
                item.setVisible(true);
            item = menu.findItem(R.id.action_search);
            if (item != null)
                item.setVisible(true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
		/* Inflate the layout for this fragment */
        View view = inflater.inflate(R.layout.fragment_root_log, container, false);

        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();

        LogFragment logFragment = new LogFragment();
        logFragment.setShouldInterfaceChange(shouldInterfaceChange);
		/*
		 * When this container fragment is created, we fill it with our first
		 * "real" fragment
		 */
        transaction.replace(R.id.root_frame, logFragment);

        transaction.commit();

        return view;
    }

    public void setShouldInterfaceChange(boolean shouldInterfaceChange)
    {
        this.shouldInterfaceChange = shouldInterfaceChange;
    }

}
