package com.seii.theandroids.myshaveoftheday.model;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import java.io.File;
import java.util.Calendar;

/**
 * Created by Clay on 2/1/2016.
 *
 * This is the model class for Shave of the Day Log entries
 */
public class LogEntry implements Comparable<LogEntry>
{
    String description, comment, image;
    Calendar date;

    /**
     * empty constructor
     */
    public LogEntry()
    {

    }

    public LogEntry(String description, String image)
    {
        this.description = description;
        this.image = image;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public LogEntry(String description, String comment, Calendar calendar, String image)
    {
        this.description = description;
        this.comment = comment;
        this.date = calendar;
        this.image = image;
    }

    public void share(Context context) // Method to share to social media. Should be expanded greatly in the future
    {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/*");
        if (!image.equals("") && new File(image).exists())
        {
            File f = new File(image);

            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
        }
        intent.putExtra(Intent.EXTRA_TEXT, description);
        context.startActivity(Intent.createChooser(intent, "Share with..."));
    }


    @Override
    public int compareTo(LogEntry otherEntry) {
        int thisYear = this.date.get(Calendar.YEAR);
        int thisMonth = this.date.get(Calendar.MONTH);
        int thisDay = this.date.get(Calendar.DAY_OF_MONTH);
        int otherYear = otherEntry.getDate().get(Calendar.YEAR);
        int otherMonth = otherEntry.getDate().get(Calendar.MONTH);
        int otherDay = otherEntry.getDate().get(Calendar.DAY_OF_MONTH);

        if (thisYear == otherYear) {
            if (thisMonth == otherMonth)
            {
                return thisDay - otherDay;
            }
            else
                return thisMonth - otherMonth;
        }
        else
            return thisYear - otherYear;


    }

    public String getDescription() {
        return description;
    }

    public String getComment() {
        return comment;
    }

    public String getImage() {
        return image;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setImage(String image) {
        this.image = image;
    }


}
