package com.seii.theandroids.myshaveoftheday;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.WindowManager;

import com.seii.theandroids.myshaveoftheday.util.ThemeUtil;

/**
 * Created by Clay on 3/5/2016.
 */
public class PreferencesActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateTheme();
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
    }

    public static class MyPreferenceFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
    {
        updateTheme();
        recreateActivity();
    }

    @Override
    protected void onResume() {
        super.onResume();
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
    }



    public void updateTheme() {
        if (ThemeUtil.getTheme(getApplicationContext()) == ThemeUtil.THEME.BLUE) {
            setTheme(R.style.MyMaterialTheme_Base);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            }
        } else if (ThemeUtil.getTheme(getApplicationContext()) == ThemeUtil.THEME.GREEN) {
            setTheme(R.style.MyMaterialTheme_Green);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.greenPrimaryDark));
            }
        }
        else if (ThemeUtil.getTheme(getApplicationContext()) == ThemeUtil.THEME.RED) {
            setTheme(R.style.MyMaterialTheme_Red);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.redPrimaryDark));
            }
        }
        else if (ThemeUtil.getTheme(getApplicationContext()) == ThemeUtil.THEME.BLACK) {
            setTheme(R.style.MyMaterialTheme_Black);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.blackPrimaryDark));
            }
        }
        else if (ThemeUtil.getTheme(getApplicationContext()) == ThemeUtil.THEME.CYAN) {
            setTheme(R.style.MyMaterialTheme_Cyan);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.cyanPrimaryDark));
            }
        }
        else if (ThemeUtil.getTheme(getApplicationContext()) == ThemeUtil.THEME.ORANGE) {
            setTheme(R.style.MyMaterialTheme_Orange);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.orangePrimaryDark));
            }
        }
        else if (ThemeUtil.getTheme(getApplicationContext()) == ThemeUtil.THEME.YELLOW) {
            setTheme(R.style.MyMaterialTheme_Yellow);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.yellowPrimaryDark));
            }
        }
        else if (ThemeUtil.getTheme(getApplicationContext()) == ThemeUtil.THEME.GREY) {
            setTheme(R.style.MyMaterialTheme_Grey);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.greyPrimaryDark));
            }
        }
        else if (ThemeUtil.getTheme(getApplicationContext()) == ThemeUtil.THEME.PURPLE) {
            setTheme(R.style.MyMaterialTheme_Purple);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.purplePrimaryDark));
            }
        }
    }
    public void recreateActivity() {
        Intent intent = getIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }

}