package com.seii.theandroids.myshaveoftheday;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.seii.theandroids.myshaveoftheday.listener.OnBackPressedListener;
import com.seii.theandroids.myshaveoftheday.model.LogEntry;
import com.seii.theandroids.myshaveoftheday.model.ShaveItem;
import com.seii.theandroids.myshaveoftheday.util.animationUtil;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * AddEntryFragment is responsible for inflating fragment_add_entry.xml and controlling the inputs for creating a new "Shave of the Day" Log entry
 *
 */
public class AddEntryFragment extends Fragment
{

    private int func;
    private String prevDesc, prevComment, prevImagePath, mCurrentPhotoPath="";
    private boolean selectedAddItemsFromShaveDen = false;

    private EditText desc, comment, etDate;
    private ImageView iv;
    private Calendar c;
    private DatePickerDialog.OnDateSetListener date;

    private LogEntry entry;
    private List<ShaveItem> items;
    private AddItemToEntryFragment dFragment;
    private DBHandler db;

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int PICK_IMAGE = 2;
    private static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;
    private static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE1 = 2;
    private static final int ADD_ENTRY = 1;
    private static final int EDIT_ENTRY = 2;
    private static final String DIRECTORY_SOTD = "SOTD";

    public AddEntryFragment()
    {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.add_entry_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu)
    {


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_done)
        {
            // adds new entry if in add mode
            if (func == ADD_ENTRY) {
                if (selectedAddItemsFromShaveDen)
                {
                    // Calls useItem() on selected ShaveItems if in add mode and user selected "Add items from Shave Den" option
                    for (int i = 0; i < items.size(); i++)
                    {
                        ShaveItem newItem = items.get(i);
                        newItem.useItem();
                        db.editShaveItem(items.get(i), newItem);
                    }
                }
                // adds new entry to database
                LogEntry entry = new LogEntry(desc.getText().toString(), comment.getText().toString(), c, mCurrentPhotoPath);
                db.addEntry(entry);
            }
            // edits existing entry if in edit mode
            else if (func == EDIT_ENTRY)
            {
                db.editLogEntry(desc.getText().toString(), comment.getText().toString(), mCurrentPhotoPath, c, prevDesc, prevComment, prevImagePath);
            }

            // Shows FAB and tabs and changes menu
            animationUtil.showTabs(getActivity());
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.SOTD);
            final MainActivity mainActivity = (MainActivity)getActivity();
            if (mainActivity.fab.getVisibility() == View.INVISIBLE)
                mainActivity.fab.setVisibility(View.VISIBLE);
            mainActivity.fab.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    animationUtil.hideTabs(mainActivity);
                    Fragment newFragment = new AddEntryFragment();
                    FragmentTransaction transaction = mainActivity.getSupportFragmentManager().beginTransaction();
                    mainActivity.getSupportActionBar().setTitle("Add new entry");
                    mainActivity.fab.setVisibility(View.INVISIBLE);
                    Bundle args = new Bundle();
                    args.putInt("FUNCTION", ADD_ENTRY);
                    newFragment.setArguments(args);
                    transaction.replace(R.id.root_frame, newFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            });

            // Goes to previous fragment
            getFragmentManager().popBackStack();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Displays dialog with ExpandableListView that lets the user select ShaveItems to be added to the description
     */
    private void displayAddItemsToEntry()
    {
        dFragment = new AddItemToEntryFragment();
        dFragment.show(getFragmentManager(), "Add Item to Entry");
        getActivity().getSupportFragmentManager().executePendingTransactions();
        final Dialog dl = dFragment.getDialog();

        Button btn = (Button) dl.findViewById(R.id.addButton);

        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                items = dFragment.getItems();
                String descStr = "";

                // Iterates through list and adds each ShaveItem's title to description
                for (int i = 0; i < items.size(); i++)
                {
                    descStr = descStr + items.get(i).getTitle();
                    if (i != items.size() - 1) {
                        descStr = descStr + "\n";
                    }
                }
                desc.setText(descStr);
                selectedAddItemsFromShaveDen = true;
                dl.dismiss();
            }
        });
    }

    /**
     * Displays a dialog that lets the user enter a description manually
     */
    private void displayAddDescription()
    {
        final AddDescriptionFragment dFragment = new AddDescriptionFragment();
        dFragment.show(getFragmentManager(), "Edit Category");
        getActivity().getSupportFragmentManager().executePendingTransactions();
        final Dialog dl = dFragment.getDialog();
        EditText editText = (EditText) dl.findViewById(R.id.descriptionTextBox);
        editText.setText(desc.getText().toString());
        Button btn = (Button)dl.findViewById(R.id.addDescriptionButton);
        btn.setText(R.string.add);

        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                desc.setText(((EditText) dl.findViewById(R.id.descriptionTextBox)).getText().toString());
                selectedAddItemsFromShaveDen = false;
                dFragment.dismiss();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_entry, container, false);

        // Checks whether it is adding an entry or editing one
        func = getArguments().getInt("FUNCTION");

        desc = (EditText) view.findViewById(R.id.desc_edit_text);
        comment = (EditText) view.findViewById(R.id.comment_edit_text);

        if (func == ADD_ENTRY)
        {
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Add entry");
        }
        else {
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Edit entry");
        }

        // Hides tabs and FAB
        final MainActivity mainActivity = (MainActivity)getActivity();
        animationUtil.hideTabs(mainActivity);
        mainActivity.fab.setVisibility(View.INVISIBLE);

        db = new DBHandler(getActivity(),null,null,1);
        if (func == EDIT_ENTRY)
        {
            ArrayList<LogEntry> entries = db.getLogEntries();
            int pos = getArguments().getInt("POSITION");
            entry = entries.get(pos);
        }

        // sets fields to current if in edit mode
        if (func == EDIT_ENTRY)
        {
            prevDesc = entry.getDescription();
            desc.setText(prevDesc);
            prevComment = entry.getComment();
            comment.setText(prevComment);
        }


        setDate(view);
        changePhoto(view);


        // Sets listener on EditText to show date picker
        desc.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setTitle(R.string.add_description);
                builder1.setCancelable(true);

                //Creates options
                builder1.setItems(R.array.entryDescOptions, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0)
                            displayAddItemsToEntry();
                        else
                            displayAddDescription();

                    }
                });

                //Creates cancel button
                builder1.setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                builder1.show();
            }
        });


//        // needs logic for add vs edit
//        OnBackPressedListener listener = new OnBackPressedListener()
//        {
//            @Override
//            public void doBack()
//            {
//
//                animationUtil.showTabs(getActivity());
//                        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.SOTD);
//
//                MainActivity mainActivity = (MainActivity)getActivity();
//                if (mainActivity.fab.getVisibility() == View.INVISIBLE)
//                    mainActivity.fab.setVisibility(View.VISIBLE);
//                mainActivity.fab.setOnClickListener(new View.OnClickListener()
//                {
//                    @Override
//                    public void onClick(View v)
//                    {
//                        animationUtil.hideTabs(getActivity());
//                        Fragment newFragment = new AddEntryFragment();
//                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
//                        transaction.replace(R.id.root_frame, newFragment);
//                        transaction.addToBackStack(null);
//                        transaction.commit();
//                    }
//                });
//            }
//        };
//        ((MainActivity) getActivity()).setOnBackPressedListener(listener);

            return view;
    }


    /**
     * Sets up OnClickListener for camera button and sets photo preview if in edit mode
     * @param v View of the fragment
     */
    private void changePhoto(View v) {

        ImageView cameraButton = (ImageView) v.findViewById(R.id.add_image);
        iv = (ImageView) v.findViewById(R.id.imagePreview);

        //sets preview to current image if in edit mode
        if (func == EDIT_ENTRY)
        {
            prevImagePath = entry.getImage();
            mCurrentPhotoPath = prevImagePath;
            setPhoto();
        }

        // Displays options dialog when camera button is clicked
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setTitle(R.string.change_photo);
                builder1.setCancelable(true);

                //Creates options
                builder1.setItems(R.array.photoOptions, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0)
                            takePhoto();
                        else
                            choosePhoto();
                    }
                });

                //Creates cancel button
                builder1.setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                builder1.show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result from image browser
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data == null)
                return;

            try {
                // Retrieves image from data in form of bitmap
                InputStream inputStream = getContext().getContentResolver().openInputStream(data.getData());
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
                Bitmap bitmap = BitmapFactory.decodeStream(bufferedInputStream);

                // Creates file path for image to be saved to
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
                String imageFileName = "JPEG_" + timeStamp;
                File storageDir = getActivity().getExternalFilesDir(DIRECTORY_SOTD);
                mCurrentPhotoPath = storageDir.getPath() + "/" + imageFileName;

                // Saves bitmap as JPEG
                FileOutputStream out = null;
                try {
                    out = new FileOutputStream(mCurrentPhotoPath);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                setPhoto();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        // Result from camera
        else if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if (resultCode == Activity.RESULT_OK) {
                setPhoto();
            }
        }
    }


    /**
     * / Sets date field to the current date (or existing LogEntry's date in edit mode) and displays date picker dialog when user clicks the field
     * @param v View of the fragment
     */
    private void setDate(View v)
    {
        //sets date field to current date
        etDate = (EditText) v.findViewById(R.id.date_edit_text);
        c = Calendar.getInstance();

        //sets date field to current if in edit mode
        if (func == EDIT_ENTRY)
        {
            c = entry.getDate();
        }

        String cStr = new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(c.getTime());
        etDate.setText(cStr);

        // Creates date picker dialog
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                c.set(Calendar.YEAR, year);
                c.set(Calendar.MONTH, monthOfYear);
                c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
                etDate.setText(sdf.format(c.getTime()));
            }
        };

        // Sets listener on EditText to show date picker
        etDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(getContext(), date,
                        c.get(Calendar.YEAR), c.get(Calendar.MONTH),
                        c.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }


    /**
     * Opens a camera activity to take a photo. Saves photo in path returned by createImageFile()
     */
    private void takePhoto()
    {
        // Checks if it has WRITE_EXTERNAL_STORAGE permission
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Displays explanation if it has one
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            } else {

                // Requests permission. onRequestPermissionsResult method handles user's input
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            }
        }

        else {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                // Create the File where the photo should go
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    // Error occurred while creating the File
                    System.out.println("Error creating image file");
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            Uri.fromFile(photoFile));

                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        }
    }


    /**
     * Creates empty file for the photo. Generates name from time the photo is taken.
     * @return Empty File where photo will be saved
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        File image = null;
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
            String imageFileName = "JPEG_" + timeStamp;
            File storageDir = getActivity().getExternalFilesDir(DIRECTORY_SOTD);
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPath = image.getAbsolutePath();
        } catch(IOException e) {
            e.printStackTrace();
        }
        return image;
    }


    /**
     * Opens a browser activity that allows a user to navigate and select a photo from storage
     */
    private void choosePhoto()
    {
         // Checks if it has WRITE_EXTERNAL_STORAGE permission
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Displays explanation if it has one
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Explanation here
            } else {
                // Requests permission. onRequestPermissionsResult method handles user's input
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE1);
            }
        }
        else
        {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
        }
    }


    /**
     * Generates a bitmap from file of mCurrentPhotoPath and sets it to the preview photo ImageView
     */
    private void setPhoto()
    {
        System.out.println("Path" + mCurrentPhotoPath);
        if (!mCurrentPhotoPath.equals("") && new File(mCurrentPhotoPath).exists()) {

            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inPurgeable = true;

            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
            iv.setImageBitmap(bitmap);
        }
    }


    /**
     * Checks if device is running Android 6.0 or higher, relevant for permission requests
     * @return Returns true if device is running Android 6.0 or higher
     */
    private boolean marshmallow() {
        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    takePhoto();

                } else {
                    // Permission denied
                    Toast.makeText(getActivity(), "Permission: WRITE_EXTERNAL_STORAGE required to add photo",
                            Toast.LENGTH_LONG).show();
                }
                return;
            }
            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    choosePhoto();

                } else {
                    // Permission denied
                    Toast.makeText(getActivity(), "Permission: WRITE_EXTERNAL_STORAGE required to add photo",
                            Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }


}
