package com.seii.theandroids.myshaveoftheday;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.FragmentManager;

import com.seii.theandroids.myshaveoftheday.adapter.CategoryAdapter;
import com.seii.theandroids.myshaveoftheday.model.ItemCategory;
import com.seii.theandroids.myshaveoftheday.model.ShaveItem;
import com.seii.theandroids.myshaveoftheday.util.animationUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Clay on 2/3/2016.
 */

public class ShaveDenFragment extends Fragment
{

    private View view;
    private CategoryAdapter adapter;
    private DBHandler db;
    private ListView lv;

    private Activity mActivity;

    List<ItemCategory> categories;

    public ShaveDenFragment()
    {

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_shave_den, container, false);
        setHasOptionsMenu(true);

        mActivity = getActivity();

        animationUtil.showTabs(mActivity);
        ((AppCompatActivity) mActivity).getSupportActionBar().setTitle(R.string.Shave_Den);
        setFloatingActionButtonAddCategory();
        if (((MainActivity)mActivity).fab.getVisibility() == View.INVISIBLE)
            ((MainActivity)mActivity).fab.setVisibility(View.VISIBLE);

        // Gets the list view for categories
        lv = (ListView) view.findViewById(R.id.categoryListView);

        db = new DBHandler(mActivity,null,null,1); // Gets the database as an object
        categories = db.getCategories();

        adapter = new CategoryAdapter(mActivity,0,categories);

        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                // Create new fragment and transaction
                ItemCategoryFragment newFragment = new ItemCategoryFragment();
                newFragment.setShaveItems(adapter.getCategoryAt(position).getShaveItems());
                newFragment.setCategory(adapter.getCategoryAt(position));

                String title = adapter.getCategoryAt(position).getTitle();


                // consider using Java coding conventions (upper first char class names!!!)
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                ((AppCompatActivity) mActivity).getSupportActionBar().setTitle(title);

                // This performs the animation for hiding the tabs. It slides the whole screen up to "hide" the tabs.
                animationUtil.hideTabs(mActivity);

                View viewpager = mActivity.findViewById(R.id.viewpager);
                animationUtil.slideFromRight(viewpager);

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                transaction.replace(R.id.shave_den_root_frame, newFragment);

                transaction.addToBackStack(null);

                // Commit the transaction
                transaction.commit();
            }
        });


        // On press and hold
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id)
            {
                final int fPosition = pos;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Choose")
                        .setItems(R.array.onLongPressOptions, new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int which)
                            {
                                final List<ItemCategory> newCategories = db.getCategories();
                                if (which == 0) // Edit
                                    displayEditCategory(newCategories.get(fPosition));
                                else if (which == 1) // Delete
                                {
                                    animationUtil.shiftFabUp(mActivity);
                                    final CoordinatorLayout coordinatorLayout = (CoordinatorLayout)view.findViewById(R.id.shave_den_layout);
                                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Category Deleted", Snackbar.LENGTH_LONG)
                                            .setAction("UNDO", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v)
                                                {
                                                    Snackbar snackbar1 = Snackbar.make(coordinatorLayout, "Category Restored",Snackbar.LENGTH_SHORT);
                                                    snackbar1.show();
                                                    animationUtil.resetFab(mActivity);

                                                    ListView lv = (ListView) view.findViewById(R.id.categoryListView);
                                                    categories = db.getCategories();
                                                    adapter = new CategoryAdapter(getActivity(), 0, categories);
                                                    lv.setAdapter(adapter);
                                                }
                                            })
                                            .setCallback(new Snackbar.Callback()
                                            {
                                                @Override
                                                public void onDismissed(Snackbar snackbar, int event)
                                                {
                                                    animationUtil.resetFab(mActivity);
                                                    if(event == Snackbar.Callback.DISMISS_EVENT_TIMEOUT)
                                                        db.deleteCategory(newCategories.get(fPosition).getTitle());
                                                }
                                            });
                                    snackbar.show();



                                    categories.remove(fPosition);
                                    ListView lv = (ListView) view.findViewById(R.id.categoryListView);
                                    List<ItemCategory> itemCategories = categories;
                                    adapter = new CategoryAdapter(getActivity(), 0, itemCategories);
                                    lv.setAdapter(adapter);
                                }
                                else if(which == 2) // Merge With
                                {
                                    Activity mainActivity = getActivity();
                                    final MergeFragment dFragment = new MergeFragment();
                                    dFragment.show(getFragmentManager(), "Merge With");
                                    getActivity().getSupportFragmentManager().executePendingTransactions();
                                    final Dialog dl = dFragment.getDialog();
                                    ListView categoryList = (ListView)dl.findViewById(R.id.mergeListView);
                                    TextView title = (TextView)dl.findViewById(R.id.mergeTitleText);
                                    title.setText("Merge " + newCategories.get(fPosition).getTitle() + " With");
                                    final DBHandler db = new DBHandler(mainActivity,null,null,1);

                                    final List<ItemCategory> categories = new ArrayList<ItemCategory>();

                                    for(ItemCategory c : db.getCategories())
                                    {
                                        if(!c.getTitle().equals(newCategories.get(fPosition).getTitle()))
                                            categories.add(c);
                                    }

                                    categoryList.setAdapter(new CategoryAdapter(mainActivity,0,categories));
                                    categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener()
                                    {
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                                        {
                                            db.deleteCategory(newCategories.get(fPosition).getTitle());
                                            db.deleteCategory(categories.get(position).getTitle());
                                            ItemCategory cat = newCategories.get(fPosition);
                                            cat.mergeWith(categories.get(position));
                                            db.addCategory(cat);
                                            for(ShaveItem item : cat.getShaveItems())
                                                db.addShaveItem(item,cat);
                                            dFragment.dismiss();

                                            List<ItemCategory> itemCategories = db.getCategories();
                                            adapter = new CategoryAdapter(getActivity(),0,itemCategories);
                                            lv.setAdapter(adapter);
                                        }
                                    });
                                }

                                if(which == 0 || which == 2) {
                                    ListView lv = (ListView) view.findViewById(R.id.categoryListView);
                                    List<ItemCategory> itemCategories = db.getCategories();
                                    adapter = new CategoryAdapter(getActivity(), 0, itemCategories);
                                    lv.setAdapter(adapter);
                                }
                            }
                        });
                Dialog d = builder.create();
                d.show();

                return true;
            }
        });

        return view;
    }


    /**
     * Displays a dialog fragment populated with the ItemCategory passed.
     * @param categoryToEdit The ItemCategory that will be changed in the database
     */
    private void displayEditCategory(final ItemCategory categoryToEdit)
    {
        final AddCategoryFragment dFragment = new AddCategoryFragment();
        dFragment.show(getFragmentManager(), "Edit Category");
        getActivity().getSupportFragmentManager().executePendingTransactions();
        final Dialog dl = dFragment.getDialog();
        Button btn = (Button)dl.findViewById(R.id.addCategoryButton);

        EditText text = (EditText)dl.findViewById(R.id.categoryTitleTextBox);
        text.setText(categoryToEdit.getTitle());
        btn.setText("Save");

        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                db.editCategory(categoryToEdit, new ItemCategory(((EditText) dl.findViewById(R.id.categoryTitleTextBox)).getText().toString()));

                ListView lv = (ListView) view.findViewById(R.id.categoryListView);
                categories = db.getCategories();
                adapter = new CategoryAdapter(getActivity(), 0, categories);
                lv.setAdapter(adapter);
                dFragment.dismiss();
            }
        });
    }


    public void setFloatingActionButtonAddCategory()
    {
        ((MainActivity)mActivity).fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final AddCategoryFragment dFragment = new AddCategoryFragment();
                dFragment.show(getFragmentManager(), "Add Category");
                ((MainActivity) mActivity).getSupportFragmentManager().executePendingTransactions();
                final Dialog dl = dFragment.getDialog();
                Button btn = (Button) dl.findViewById(R.id.addCategoryButton);
                btn.setText("Add");

                btn.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if (!((EditText) dl.findViewById(R.id.categoryTitleTextBox)).getText().toString().isEmpty())
                        {
                            DBHandler db = new DBHandler(mActivity, null, null, 1);
                            db.addCategory(new ItemCategory(((EditText) dl.findViewById(R.id.categoryTitleTextBox)).getText().toString()));
                            ListView lv = (ListView) mActivity.findViewById(R.id.categoryListView);
                            categories = db.getCategories();
                            adapter = new CategoryAdapter(mActivity, 0, categories);
                            lv.setAdapter(adapter);
                            dFragment.dismiss();
                        }
                    }
                });
            }
        });
    }
}



