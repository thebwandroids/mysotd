package com.seii.theandroids.myshaveoftheday.model;

/**
 * This class provides convenient types for shave items with no real value or function.
 * This simply provides a universal convention for classifying shave items.
 */
public enum ITEM_TYPE
{
    REGULAR(0),
    DISPOSABLE(1),
    CONSUMABLE(2);

    private int value;

    ITEM_TYPE(int value)
    {
        this.value = value;
    }

    public int getValue()
    {
        return this.value;
    }

    public static ITEM_TYPE getTypeFromValue(int value)
    {
        ITEM_TYPE type = null;
        if(value == 0)
            type = REGULAR;
        else if(value == 1)
            type = DISPOSABLE;
        else if(value == 2)
            type = CONSUMABLE;
        return type;
    }
}
