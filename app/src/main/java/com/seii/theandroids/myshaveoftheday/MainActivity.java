package com.seii.theandroids.myshaveoftheday;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import com.seii.theandroids.myshaveoftheday.adapter.CategoryAdapter;
import com.seii.theandroids.myshaveoftheday.listener.OnBackPressedListener;
import com.seii.theandroids.myshaveoftheday.model.ConsumableShaveItem;
import com.seii.theandroids.myshaveoftheday.model.DisposableShaveItem;
import com.seii.theandroids.myshaveoftheday.model.ItemCategory;
import com.seii.theandroids.myshaveoftheday.model.LogEntry;
import com.seii.theandroids.myshaveoftheday.model.RegularShaveItem;
import com.seii.theandroids.myshaveoftheday.util.ThemeUtil;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * This is the main activity where the application starts. This activity holds nearly all the fragments of the app besides the preferences.
 */
public class MainActivity extends AppCompatActivity
{
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    public FloatingActionButton fab;

    protected OnBackPressedListener onBackPressedListener;

    private static final int ADD_ENTRY = 1;

    MainActivity activity;

    private RootShaveDenFragment mShaveDenFragment;
    private RootLogFragment mLogFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        activity = this;
        updateTheme();
        setContentView(R.layout.activity_main);

        setDefaultCategories();

        // Grabs the Floating Action Button
        fab = (FloatingActionButton) findViewById(R.id.fab);

        // Tells the app to set the default action bar to the toolbar we created in activity_main.xml
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Sets the viewpager up with an adapter and fragments to switch between
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
                //invalidateOptionsMenu();
            }

            @Override
            public void onPageSelected(int position)
            {
                if (position == 0)
                {
                    toolbar.setTitle(R.string.Shave_Den);
                    mShaveDenFragment.setFloatingActionButtonAddCategory();
                }
                else
                {
                    toolbar.setTitle(R.string.SOTD);
                    fab.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            //animationUtil.hideTabs(activity);
                            Fragment newFragment = new AddEntryFragment();
                            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                            //getSupportActionBar().setTitle("Add new entry");
                            //fab.setVisibility(View.INVISIBLE);
                            Bundle args = new Bundle();
                            args.putInt("FUNCTION", ADD_ENTRY);
                            newFragment.setArguments(args);
                            transaction.replace(R.id.root_frame, newFragment);
                            transaction.addToBackStack(null);
                            transaction.commit();
                        }
                    });
                }
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {

            }
        });

        // Finally, attaches the tab layout with the viewpager
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            Intent i = new Intent(this, PreferencesActivity.class);
            startActivity(i);
            inSettings = true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * This code will check if the app is being ran for the first time. If so, it creates default shave categories
     */
    private void setDefaultCategories()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean firstTime = prefs.getBoolean("first", true);

        if(firstTime)
        {
            DBHandler db = new DBHandler(this,null,null,1);
            db.addCategory(new ItemCategory("Razors"));
            db.addCategory(new ItemCategory("Brushes"));
            db.addCategory(new ItemCategory("Soaps"));
            db.addCategory(new ItemCategory("Creams"));
            db.addCategory(new ItemCategory("Aftershaves"));
            db.addCategory(new ItemCategory("Pre-shaves"));
            db.addCategory(new ItemCategory("Blades"));
            prefs.edit().putBoolean("first",false).apply();
        }
    }

    /**
     * Adds the two main fragments to the view pager
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        mLogFragment = new RootLogFragment();
        mLogFragment.setShouldInterfaceChange(false);
        mShaveDenFragment = new RootShaveDenFragment();
        adapter.addFragment(mShaveDenFragment, "Shave Den");
        adapter.addFragment(mLogFragment, "Shave Of The Day");
        viewPager.setAdapter(adapter);
    }


    /**
     * Sets the listener for the back pressed event.
     * @param onBackPressedListener
     */
    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

    @Override
    public void onBackPressed()
    {
        // This code manages the onBackPress event, which may change depending on the current fragment
        try
        {
            if (onBackPressedListener != null)
                onBackPressedListener.doBack();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        super.onBackPressed();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    private boolean inSettings = false;
    @Override
    protected void onRestart() {
        super.onRestart();
        if(inSettings)
        {
            recreateActivity();
            inSettings = false;
        }
    }

    /**
     * This practically restarts the app. This code is required to run when the theme changes
     */
    public void recreateActivity() {
        Intent intent = getIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }

    /**
     * This code updates the theme of the application
     */
    public void updateTheme() {
        if (ThemeUtil.getTheme(getApplicationContext()) == ThemeUtil.THEME.BLUE) {
            setTheme(R.style.MyMaterialTheme_Base);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            }
        } else if (ThemeUtil.getTheme(getApplicationContext()) == ThemeUtil.THEME.GREEN) {
            setTheme(R.style.MyMaterialTheme_Green);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.greenPrimaryDark));
            }
        }
        else if (ThemeUtil.getTheme(getApplicationContext()) == ThemeUtil.THEME.RED) {
            setTheme(R.style.MyMaterialTheme_Red);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.redPrimaryDark));
            }
        }
        else if (ThemeUtil.getTheme(getApplicationContext()) == ThemeUtil.THEME.BLACK) {
            setTheme(R.style.MyMaterialTheme_Black);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.blackPrimaryDark));
            }
        }
        else if (ThemeUtil.getTheme(getApplicationContext()) == ThemeUtil.THEME.CYAN) {
            setTheme(R.style.MyMaterialTheme_Cyan);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.cyanPrimaryDark));
            }
        }
        else if (ThemeUtil.getTheme(getApplicationContext()) == ThemeUtil.THEME.ORANGE) {
            setTheme(R.style.MyMaterialTheme_Orange);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.orangePrimaryDark));
            }
        }
        else if (ThemeUtil.getTheme(getApplicationContext()) == ThemeUtil.THEME.YELLOW) {
            setTheme(R.style.MyMaterialTheme_Yellow);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.yellowPrimaryDark));
            }
        }
        else if (ThemeUtil.getTheme(getApplicationContext()) == ThemeUtil.THEME.GREY) {
            setTheme(R.style.MyMaterialTheme_Grey);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.greyPrimaryDark));
            }
        }
        else if (ThemeUtil.getTheme(getApplicationContext()) == ThemeUtil.THEME.PURPLE) {
            setTheme(R.style.MyMaterialTheme_Purple);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.purplePrimaryDark));
            }
        }
    }

    /**
     * Adapter for the viewpager, which handles the tabs
     */
    class ViewPagerAdapter extends FragmentPagerAdapter
    {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
