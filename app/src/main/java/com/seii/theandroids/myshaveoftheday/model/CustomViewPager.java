package com.seii.theandroids.myshaveoftheday.model;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewPropertyAnimator;

/**
 * This class is the same as the default viewpager, with added functionality specific for the application
 */
public class CustomViewPager extends ViewPager
{
    private boolean swipeable;

    public CustomViewPager(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.swipeable = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return this.swipeable ? super.onTouchEvent(event) : false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return this.swipeable ? super.onInterceptTouchEvent(event) : false;
    }

    public void setPagingEnabled(boolean enabled) {
        this.swipeable = enabled;
    }

    public boolean isPagingEnabled() {
        return this.swipeable;
    }
}
