package com.seii.theandroids.myshaveoftheday.model;

import com.seii.theandroids.myshaveoftheday.R;

import java.util.Calendar;

/**
 *
 */
public class RegularShaveItem implements ShaveItem
{
    private String title;
    private String image;
    private Calendar purchaseDate;
    private double purchasePrice;
    private Calendar lastUse;

    private String note;

    //empty constructor
    public RegularShaveItem()
    {
        purchaseDate = Calendar.getInstance();
        lastUse = Calendar.getInstance();
        image = "";
    }
    public RegularShaveItem(String title) {
        this.title = title;
        purchaseDate = Calendar.getInstance();
        lastUse = Calendar.getInstance();
        image = "";
    }

    public RegularShaveItem(String title, String image)
    {
        this.title = title;
        purchaseDate = Calendar.getInstance();
        lastUse = Calendar.getInstance();
        this.image = image;
    }
    @Override
    public int getMenu()
    {
        return R.array.onLongPressShaveItem;
    }

    @Override
    public ITEM_TYPE getType()
    {
        return ITEM_TYPE.REGULAR;
    }

    @Override
    public String getTitle()
    {
        return title;
    }

    @Override
    public Calendar getPurchaseDate()
    {
        return purchaseDate;
    }

    @Override
    public double getPurchasePrice()
    {
        return purchasePrice;
    }

    @Override
    public Calendar getLastUse()
    {
        return lastUse;
    }

    @Override
    public String getNote()
    {
        return note;
    }

    @Override
    public String getImage() {return image; }

    @Override
    public void setTitle(String title)
    {
        this.title = title;
    }

    @Override
    public void setImage(String image)
    {
        this.image = image;
    }

    @Override
    public void setPurchaseDate(Calendar purchaseDate)
    {
        this.purchaseDate = purchaseDate;
    }

    @Override
    public void setPurchasePrice(double purchasePrice)
    {
        this.purchasePrice = purchasePrice;
    }

    @Override
    public void setLastUse(Calendar lastUse)
    {
        this.lastUse = lastUse;
    }

    @Override
    public void setNote(String note)
    {
        this.note = note;
    }

    @Override
    public void useItem()
    {

    }
}
