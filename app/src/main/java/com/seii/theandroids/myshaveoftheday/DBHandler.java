package com.seii.theandroids.myshaveoftheday;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.seii.theandroids.myshaveoftheday.model.ConsumableShaveItem;
import com.seii.theandroids.myshaveoftheday.model.DisposableShaveItem;
import com.seii.theandroids.myshaveoftheday.model.ITEM_TYPE;
import com.seii.theandroids.myshaveoftheday.model.ItemCategory;
import com.seii.theandroids.myshaveoftheday.model.LogEntry;
import com.seii.theandroids.myshaveoftheday.model.RegularShaveItem;
import com.seii.theandroids.myshaveoftheday.model.ShaveItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

/**
 * Created by Clay on 3/2/2016.
 */
public class DBHandler extends SQLiteOpenHelper
{
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "MySOTD.db";

    private static final String TABLE_CATEGORIES = "Categories";
    private static final String COLUMN_CATEGORY_ID = "Category_ID";
    private static final String COLUMN_CATEGORY_TITLE = "Category_Title";

    private static final String TABLE_ITEMS = "Items";
    private static final String COLUMN_ITEM_ID = "Item_ID";
    private static final String COLUMN_ITEM_TITLE = "Item_title";
    private static final String COLUMN_ITEM_PURCHASE_DAY = "Purchase_day";
    private static final String COLUMN_ITEM_PURCHASE_MONTH = "Purchase_month";
    private static final String COLUMN_ITEM_PURCHASE_YEAR = "Purchase_year";
    private static final String COLUMN_ITEM_PRICE = "Price";
    private static final String COLUMN_ITEM_LAST_DAY = "Last_day";
    private static final String COLUMN_ITEM_LAST_MONTH = "Last_month";
    private static final String COLUMN_ITEM_LAST_YEAR = "Last_year";
    private static final String COLUMN_ITEM_IMAGE = "Item_image";
    private static final String COLUMN_ITEM_NOTE = "Note";
    private static final String COLUMN_ITEM_TYPE = "Type";
    private static final String COLUMN_ITEM_USES = "Uses";
    private static final String COLUMN_ITEM_UNITS = "Units";



    private static final String TABLE_SOTD_ENTRIES = "SOTD_Entries";
    private static final String COLUMN_ENTRY_ID = "Entry_ID";
    private static final String COLUMN_ENTRY_DESC = "Entry_Description";
    private static final String COLUMN_ENTRY_IMAGE = "Entry_image";
    private static final String COLUMN_ENTRY_COMMENT = "Entry_comment";
    private static final String COLUMN_ENTRY_DAY = "Entry_day";
    private static final String COLUMN_ENTRY_MONTH = "Entry_month";
    private static final String COLUMN_ENTRY_YEAR = "Entry_year";


    public DBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        // Create Categories Table
        db.execSQL("CREATE TABLE " + TABLE_CATEGORIES + "(" +
                COLUMN_CATEGORY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CATEGORY_TITLE + " TEXT" +
                ");");

        // Create Items Table
        db.execSQL("CREATE TABLE " + TABLE_ITEMS + "(" +
                COLUMN_ITEM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CATEGORY_ID + " INTEGER, " +
                COLUMN_ITEM_TITLE + " TEXT, " +
                COLUMN_ITEM_LAST_DAY + " INTEGER, " +
                COLUMN_ITEM_LAST_MONTH + " INTEGER, " +
                COLUMN_ITEM_LAST_YEAR + " INTEGER, " +
                COLUMN_ITEM_PURCHASE_DAY + " INTEGER, " +
                COLUMN_ITEM_PURCHASE_MONTH + " INTEGER, " +
                COLUMN_ITEM_PURCHASE_YEAR + " INTEGER, " +
                COLUMN_ITEM_PRICE + " TEXT, " +
                COLUMN_ITEM_IMAGE + " TEXT, " +
                COLUMN_ITEM_NOTE + " TEXT, " +
                COLUMN_ITEM_TYPE + " INTEGER, " +
                COLUMN_ITEM_USES + " INTEGER, " +
                COLUMN_ITEM_UNITS + " INTEGER" +
                ");");

        // Create Entries Table
        db.execSQL("CREATE TABLE " + TABLE_SOTD_ENTRIES + "(" +
                COLUMN_ENTRY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_ENTRY_DESC + " TEXT, " +
                COLUMN_ENTRY_IMAGE + " TEXT, " +
                COLUMN_ENTRY_COMMENT + " TEXT, " +
                COLUMN_ENTRY_DAY + " INTEGER, " +
                COLUMN_ENTRY_MONTH + " INTEGER, " +
                COLUMN_ENTRY_YEAR + " INTEGER" +
                ");");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }

    public ArrayList<LogEntry> searchEntries(String s)
    {
        String desc;
        ArrayList<LogEntry> entries = getLogEntries();
        ArrayList<LogEntry> results = new ArrayList<>();
        for (int i = 0; i < entries.size(); i++)
        {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            String date = sdf.format(entries.get(i).getDate().getTime());
            desc = entries.get(i).getDescription() + entries.get(i).getComment() + date;
            desc = desc.replaceAll("\\s", "");
            if (desc.toLowerCase().contains(s.toLowerCase()))
                results.add(entries.get(i));
        }
        return results;
    }


    public void addCategory(ItemCategory category)
    {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CATEGORY_TITLE, category.getTitle());
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_CATEGORIES, null, values);
        db.close();
    }

    public void addShaveItem(ShaveItem item, ItemCategory category)
    {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CATEGORY_ID, findCategoryID(category.getTitle()));
        values.put(COLUMN_ITEM_TITLE, item.getTitle());
        values.put(COLUMN_ITEM_LAST_DAY, item.getLastUse().get(Calendar.DAY_OF_MONTH));
        values.put(COLUMN_ITEM_LAST_MONTH, item.getLastUse().get(Calendar.MONTH));
        values.put(COLUMN_ITEM_LAST_YEAR, item.getLastUse().get(Calendar.YEAR));
        values.put(COLUMN_ITEM_PURCHASE_DAY, item.getPurchaseDate().get(Calendar.DAY_OF_MONTH));
        values.put(COLUMN_ITEM_PURCHASE_MONTH, item.getPurchaseDate().get(Calendar.MONTH));
        values.put(COLUMN_ITEM_PURCHASE_YEAR, item.getPurchaseDate().get(Calendar.YEAR));
        values.put(COLUMN_ITEM_IMAGE, item.getImage());
        values.put(COLUMN_ITEM_PRICE, (item.getPurchasePrice()+""));
        values.put(COLUMN_ITEM_NOTE, item.getNote());
        values.put(COLUMN_ITEM_TYPE, item.getType().getValue());
        values.put(COLUMN_ITEM_USES, item.getType() == ITEM_TYPE.CONSUMABLE ? ((ConsumableShaveItem)item).getNumberOfUses() : 0);
        values.put(COLUMN_ITEM_UNITS, item.getType() == ITEM_TYPE.DISPOSABLE ? ((DisposableShaveItem)item).getUnitsLeft() : 0);

        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_ITEMS, null, values);
        db.close();
    }

    public void addEntry(LogEntry entry)
    {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ENTRY_DESC, entry.getDescription());
        values.put(COLUMN_ENTRY_IMAGE, entry.getImage());
        values.put(COLUMN_ENTRY_COMMENT, entry.getComment());
        values.put(COLUMN_ENTRY_DAY, entry.getDate().get(Calendar.DAY_OF_MONTH));
        values.put(COLUMN_ENTRY_MONTH, entry.getDate().get(Calendar.MONTH));
        values.put(COLUMN_ENTRY_YEAR, entry.getDate().get(Calendar.YEAR));
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_SOTD_ENTRIES, null, values);
        db.close();
    }

    //DELETE FUNCTIONALITY WITHIN SQL_liteDB
    public void deleteCategory(String title)
    {
        SQLiteDatabase db = getWritableDatabase();
        int id = findCategoryID(title);
        db.delete(TABLE_CATEGORIES, COLUMN_CATEGORY_TITLE + "='" + title + "'", null);
        db.delete(TABLE_ITEMS, COLUMN_CATEGORY_ID + "=" + id, null);
    }

    public boolean deleteShaveItem(String title)
    {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_ITEMS, COLUMN_ITEM_TITLE + "='" + title + "'", null) > 0;
    }

    public boolean deleteLogEntry(String description, String image, String comment)
    {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_SOTD_ENTRIES,
                COLUMN_ENTRY_DESC + "='" + description + "'" + " AND "+
                        COLUMN_ENTRY_IMAGE + "='" + image + "'"+ " AND " +
                        COLUMN_ENTRY_COMMENT + "='" + comment + "'",
                null) > 0;
    }

    public void editCategory(ItemCategory oldCategory, ItemCategory newCategory)
    {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CATEGORY_TITLE, newCategory.getTitle());
        SQLiteDatabase db = getWritableDatabase();
        db.update(TABLE_CATEGORIES,values,COLUMN_CATEGORY_ID + "='" + findCategoryID(oldCategory.getTitle()) +"'",null);
        db.close();
    }

    public void editShaveItem(ShaveItem oldItem, ShaveItem newItem)
    {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ITEM_TITLE, newItem.getTitle());
        values.put(COLUMN_ITEM_LAST_DAY, newItem.getLastUse().get(Calendar.DAY_OF_MONTH));
        values.put(COLUMN_ITEM_LAST_MONTH, newItem.getLastUse().get(Calendar.MONTH));
        values.put(COLUMN_ITEM_LAST_YEAR, newItem.getLastUse().get(Calendar.YEAR));
        values.put(COLUMN_ITEM_PURCHASE_DAY, newItem.getPurchaseDate().get(Calendar.DAY_OF_MONTH));
        values.put(COLUMN_ITEM_PURCHASE_MONTH, newItem.getPurchaseDate().get(Calendar.MONTH));
        values.put(COLUMN_ITEM_PURCHASE_YEAR, newItem.getPurchaseDate().get(Calendar.YEAR));
        values.put(COLUMN_ITEM_IMAGE, newItem.getImage());
        values.put(COLUMN_ITEM_PRICE, newItem.getPurchasePrice());
        values.put(COLUMN_ITEM_NOTE, newItem.getNote());
        values.put(COLUMN_ITEM_TYPE, newItem.getType().getValue());
        values.put(COLUMN_ITEM_USES, newItem.getType() == ITEM_TYPE.CONSUMABLE ? ((ConsumableShaveItem)newItem).getNumberOfUses() : 0);
        values.put(COLUMN_ITEM_UNITS, newItem.getType() == ITEM_TYPE.DISPOSABLE ? ((DisposableShaveItem) newItem).getUnitsLeft() : 0);

        SQLiteDatabase db = getWritableDatabase();
        db.update(TABLE_ITEMS, values, COLUMN_ITEM_ID + "='" + findItemID(oldItem.getTitle()) + "'", null);
    }

    public void editLogEntry(String description, String comment, String image, Calendar date, String prevDesc, String prevComment, String prevImage)
    {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ENTRY_DESC, description);
        values.put(COLUMN_ENTRY_IMAGE, image);
        values.put(COLUMN_ENTRY_COMMENT, comment);
        values.put(COLUMN_ENTRY_DAY, date.get(Calendar.DAY_OF_MONTH));
        values.put(COLUMN_ENTRY_MONTH, date.get(Calendar.MONTH));
        values.put(COLUMN_ENTRY_YEAR, date.get(Calendar.YEAR));
        SQLiteDatabase db = getWritableDatabase();

        // Runs query to calculate id
        Cursor c = db.rawQuery("SELECT " + COLUMN_ENTRY_ID +
                " FROM " + TABLE_SOTD_ENTRIES +
                " WHERE " + COLUMN_ENTRY_DESC + "='" + prevDesc + "'" +
                " AND " + COLUMN_ENTRY_IMAGE + "='" + prevImage + "'" +
                " AND " + COLUMN_ENTRY_COMMENT + "='" + prevComment + "'", null);
        c.moveToFirst();
        String id = (c.getString(c.getColumnIndex(COLUMN_ENTRY_ID)));

        db.update(TABLE_SOTD_ENTRIES, values, COLUMN_ENTRY_ID + "='" + id + "'", null);
    }

    /**
     *
     * @return An arraylist of all the categories in the database, including their shave items
     */
    public ArrayList<ItemCategory> getCategories()
    {
        ArrayList<ItemCategory> categories = new ArrayList<ItemCategory>();

        SQLiteDatabase db = getWritableDatabase();
        String getAllQuery = "SELECT * FROM ";
        Cursor categoryCursor = db.rawQuery(getAllQuery + TABLE_CATEGORIES, null),
                itemCursor = db.rawQuery(getAllQuery + TABLE_ITEMS, null);

        categoryCursor.moveToFirst();
        itemCursor.moveToFirst();

        if(categoryCursor.getCount() > 0) // If category table is not empty
        {
            ItemCategory category = new ItemCategory();
            do
            {
                category.setTitle(categoryCursor.getString(categoryCursor.getColumnIndex(COLUMN_CATEGORY_TITLE)));
                categories.add(category);

                if(itemCursor.getCount() >0) // If item table is not empty
                {
                    ShaveItem item = null;
                    do
                    {
                        if(categoryCursor.getString(categoryCursor.getColumnIndex(COLUMN_CATEGORY_ID)).equals(itemCursor.getString(itemCursor.getColumnIndex(COLUMN_CATEGORY_ID))))
                        {
                            ITEM_TYPE type = ITEM_TYPE.getTypeFromValue(itemCursor.getInt(itemCursor.getColumnIndex(COLUMN_ITEM_TYPE)));
                            if(type == ITEM_TYPE.REGULAR)
                                item = new RegularShaveItem();
                            else if(type == ITEM_TYPE.DISPOSABLE)
                            {
                                item = new DisposableShaveItem();
                                ((DisposableShaveItem)item).setUnitsLeft(itemCursor.getInt(itemCursor.getColumnIndex(COLUMN_ITEM_UNITS)));
                            }
                            else if(type == ITEM_TYPE.CONSUMABLE)
                            {
                                item = new ConsumableShaveItem();
                                ((ConsumableShaveItem)item).setNumberOfUses(itemCursor.getInt(itemCursor.getColumnIndex(COLUMN_ITEM_USES)));
                            }

                            item.setTitle(itemCursor.getString(itemCursor.getColumnIndex(COLUMN_ITEM_TITLE)));
                            item.setImage(itemCursor.getString(itemCursor.getColumnIndex(COLUMN_ITEM_IMAGE)));

                            Calendar c = Calendar.getInstance();
                            c.set(itemCursor.getInt(itemCursor.getColumnIndex(COLUMN_ITEM_LAST_YEAR)),
                                    itemCursor.getInt(itemCursor.getColumnIndex(COLUMN_ITEM_LAST_MONTH)),
                                    itemCursor.getInt(itemCursor.getColumnIndex(COLUMN_ITEM_LAST_DAY)),
                                    0, 0);
                            item.setLastUse(c);

                            item.setNote(itemCursor.getString(itemCursor.getColumnIndex(COLUMN_ITEM_NOTE)));

                            Calendar d = Calendar.getInstance();
                            d.set(itemCursor.getInt(itemCursor.getColumnIndex(COLUMN_ITEM_PURCHASE_YEAR)),
                                    itemCursor.getInt(itemCursor.getColumnIndex(COLUMN_ITEM_PURCHASE_MONTH)),
                                    itemCursor.getInt(itemCursor.getColumnIndex(COLUMN_ITEM_PURCHASE_DAY)),
                                    0, 0);
                            item.setPurchaseDate(d);

                            item.setPurchasePrice(Double.parseDouble(itemCursor.getString(itemCursor.getColumnIndex(COLUMN_ITEM_PRICE))));

                            category.addItem(item);
                        }

                    } while (itemCursor.moveToNext());
                }

                category = new ItemCategory();
                itemCursor.moveToFirst();

            } while (categoryCursor.moveToNext());
        }

        return categories;
    }


    /**
     *
     * @return An arraylist of all the SOTD log entries in the database
     */
    public ArrayList<LogEntry> getLogEntries() {

        ArrayList<LogEntry> entries = new ArrayList<>();

        SQLiteDatabase db = getWritableDatabase();
        String getAllQuery = "SELECT * FROM ";
        Cursor entryCursor = db.rawQuery(getAllQuery + TABLE_SOTD_ENTRIES, null);
        entryCursor.moveToFirst();

        if(entryCursor.getCount() > 0) // If entry table is not empty
        {
            LogEntry entry = new LogEntry();
            do
            {
                entry.setDescription(entryCursor.getString(entryCursor.getColumnIndex(COLUMN_ENTRY_DESC)));
                entry.setComment(entryCursor.getString(entryCursor.getColumnIndex(COLUMN_ENTRY_COMMENT)));
                entry.setImage(entryCursor.getString(entryCursor.getColumnIndex(COLUMN_ENTRY_IMAGE)));

                Calendar c = Calendar.getInstance();
                c.set(entryCursor.getInt(entryCursor.getColumnIndex(COLUMN_ENTRY_YEAR)),
                        entryCursor.getInt(entryCursor.getColumnIndex(COLUMN_ENTRY_MONTH)),
                        entryCursor.getInt(entryCursor.getColumnIndex(COLUMN_ENTRY_DAY)),
                        0, 0);
                entry.setDate(c);


                entries.add(entry);

                entry = new LogEntry();

            } while (entryCursor.moveToNext());
        }

        Collections.sort(entries);
        Collections.reverse(entries);

        return entries;

    }


    /**
     * Finds and returns the category ID that is requested
     * @param title The title (name) of the category that is being searched for
     * @return The ID of the found category
     */
    private int findCategoryID(String title)
    {
        SQLiteDatabase db = getWritableDatabase();

        // This query grabs all the categories with the same name as the parameter (which should only be one)
        String query = "SELECT " + COLUMN_CATEGORY_ID + " FROM " + TABLE_CATEGORIES + " WHERE " + COLUMN_CATEGORY_TITLE + " = '" + title + "';";

        // The cursor is moved to the first row (should be the only row)
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();

        // Returns the ID of the row
        return c.getInt(c.getColumnIndex(COLUMN_CATEGORY_ID));
    }

    private String findItemID(String title)
    {
        SQLiteDatabase db = getWritableDatabase();

        // This query grabs all the categories with the same name as the parameter (which should only be one)
        String query = "SELECT " + COLUMN_ITEM_ID + " FROM " + TABLE_ITEMS + " WHERE " + COLUMN_ITEM_TITLE + " = '" + title + "';";

        // The cursor is moved to the first row (should be the only row)
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        // Returns the ID of the row
        return c.getString(c.getColumnIndex(COLUMN_ITEM_ID));
    }

    public void clearDB()
    {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SOTD_ENTRIES);
        onCreate(db);
    }



}
