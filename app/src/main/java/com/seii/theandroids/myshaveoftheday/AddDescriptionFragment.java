package com.seii.theandroids.myshaveoftheday;

import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * AddDescriptionFragment is a dialog with an editable text field for adding a manual description.
 */
public class AddDescriptionFragment extends DialogFragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_description, container, false);
    }

}
