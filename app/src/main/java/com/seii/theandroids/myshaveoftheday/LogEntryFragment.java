package com.seii.theandroids.myshaveoftheday;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.seii.theandroids.myshaveoftheday.listener.OnBackPressedListener;
import com.seii.theandroids.myshaveoftheday.model.LogEntry;
import com.seii.theandroids.myshaveoftheday.util.animationUtil;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;


/**
 * LogEntryFragment is responsible for providing an expanded view of a LogEntry, and giving the user the option to edit, delete, or share the LogEntry.
 */
public class LogEntryFragment extends Fragment {

    private int pos;
    private DBHandler db;
    private LogEntry entry;
    private ImageView imgImageView;

    private static final int ADD_ENTRY = 1;
    private static final int EDIT_ENTRY = 2;

    public LogEntryFragment()
    {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        menu.clear();
        inflater.inflate(R.menu.log_entry_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_share) {
            entry.share(getActivity());
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_log_entry, container, false);
        setHasOptionsMenu(true);


        // Gets LogEntry from database using POSITION argument
        db = new DBHandler(getActivity(),null,null,1);
        pos = getArguments().getInt("POSITION");
        ArrayList<LogEntry> entries = db.getLogEntries();
        entry = entries.get(pos);

        final MainActivity mainActivity = (MainActivity)getActivity();
        final View tabs = mainActivity.findViewById(R.id.tabs);
        animationUtil.hideTabs(mainActivity);
        if (mainActivity.fab.getVisibility() == View.VISIBLE)
            mainActivity.fab.setVisibility(View.INVISIBLE);

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        ((AppCompatActivity) mainActivity).getSupportActionBar().setTitle(sdf.format(entry.getDate().getTime()));


        setLayout(view);

        // Runs after view is done being created so that imageView.getWidth() and getHeight() don't result in 0
        view.post(new Runnable() {
            @Override
            public void run() {
                setPhoto(entry.getImage(), imgImageView);
            }
        });

        // Edit entry
        Button edit = (Button) view.findViewById(R.id.editButton);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // When clicked, opens a new fragment with a more detailed view of the entry
                Fragment newFragment = new AddEntryFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                Bundle args = new Bundle();

                // Tells AddEntryFragment that it is editing an entry
                args.putInt("FUNCTION", EDIT_ENTRY);

                //Sends position to AddEntryFragment
                args.putInt("POSITION", pos);

                newFragment.setArguments(args);
                transaction.replace(R.id.root_frame, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });

        // Delete entry
        Button delete = (Button) view.findViewById(R.id.deleteButton);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // When clicked, deletes entry in the database, and pops BackStack
                LogEntry entryToDelete = entry;
                db.deleteLogEntry(entryToDelete.getDescription(), entryToDelete.getImage(), entryToDelete.getComment());
                animationUtil.showTabs(getActivity());
                ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.SOTD);
                final MainActivity mainActivity = (MainActivity)getActivity();
                mainActivity.fab.setVisibility(View.VISIBLE);
                getFragmentManager().popBackStack();
            }
        });

        // Back press listener
        OnBackPressedListener listener = new OnBackPressedListener()
        {
            @Override
            public void doBack()
            {
                animationUtil.showTabs(getActivity());
                ((MainActivity) getActivity()).setOnBackPressedListener(null);
                ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.SOTD);

                final MainActivity mainActivity = (MainActivity)getActivity();
                mainActivity.fab.setVisibility(View.VISIBLE);
                mainActivity.fab.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        animationUtil.hideTabs(mainActivity);
                        Fragment newFragment = new AddEntryFragment();
                        FragmentTransaction transaction = mainActivity.getSupportFragmentManager().beginTransaction();
                        mainActivity.getSupportActionBar().setTitle("Add new entry");
                        mainActivity.fab.setVisibility(View.INVISIBLE);
                        Bundle args = new Bundle();
                        args.putInt("FUNCTION", ADD_ENTRY);
                        newFragment.setArguments(args);
                        transaction.replace(R.id.root_frame, newFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                });
            }
        };
        ((MainActivity) getActivity()).setOnBackPressedListener(listener);
        getActivity().findViewById(R.id.tabs).setVisibility(View.VISIBLE);

        return view;
    }

    /**
     * Retrieves data from LogEntry and populates the layout with it
     * @param v View of the fragment
     */
    private void setLayout(View v)
    {
        Calendar c = entry.getDate();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
        String date = sdf.format(c.getTime());

        //Populates layout with LogEntry's data
        TextView descTextView = (TextView) v.findViewById(R.id.descTextView);
        descTextView.setText(entry.getDescription());
        TextView commentTextView = (TextView) v.findViewById(R.id.commentTextView);
        commentTextView.setText(entry.getComment());
        TextView dateTextView = (TextView) v.findViewById(R.id.dateTextView);
        dateTextView.setText(date);
        imgImageView = (ImageView) v.findViewById(R.id.logImageView);

    }

    /**
     * Generates a bitmap from file of mCurrentPhotoPath and sets it to the preview photo ImageView
     */
    private void setPhoto(String path, ImageView iv)
    {
        if (!path.equals("") && new File(path).exists()) {

            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, bmOptions);
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inPurgeable = true;

            Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
            iv.setImageBitmap(bitmap);
        }
    }


}
