package com.seii.theandroids.myshaveoftheday.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import com.seii.theandroids.myshaveoftheday.R;
import com.seii.theandroids.myshaveoftheday.model.CustomViewPager;

/**
 * This class provides a collection of static methods for animations that are performed in multiple places within the application
 */
public class animationUtil
{
    public static void hideTabs(Activity activity)
    {
        if(activity == null)
            return;

        // Grabs the tab bar
        final View tabView = activity.findViewById(R.id.tabs);

        // Grabs the tab bar and the toolbar as one entity
        final View mainView = activity.findViewById(R.id.top_bars);

        // Animates the bars up, pushing the tab bar off screen
        mainView.animate().translationY(-tabView.getHeight()).setDuration(200);

        // Animates the bars up while setting the tabs to disappear. This doesn't move anything, but rather keeps the whole view on screen
        mainView.animate().translationY(-tabView.getHeight()).setDuration(200).setListener(new AnimatorListenerAdapter()
        {
            @Override
            public void onAnimationEnd(Animator animation)
            {
                super.onAnimationEnd(animation);
                mainView.setTranslationY(0);
                tabView.setVisibility(View.GONE);
            }
        });

        // Disables the tabs to be swiped due to them being out of view
        CustomViewPager viewpager = (CustomViewPager)activity.findViewById(R.id.viewpager);
        viewpager.setPagingEnabled(false);
    }

    public static void showTabs(Activity activity)
    {
        final View tabView = activity.findViewById(R.id.tabs);
        final View mainView = activity.findViewById(R.id.top_bars);
        mainView.setTranslationY(-tabView.getHeight());
        tabView.setVisibility(View.VISIBLE);

        mainView.animate().translationY(0).setDuration(200).setListener(new AnimatorListenerAdapter()
        {
            @Override
            public void onAnimationCancel(Animator animation)
            {
                super.onAnimationCancel(animation);
            }
        });

        // Enables the tabs to be swiped across since they are now in view
        CustomViewPager viewpager = (CustomViewPager)activity.findViewById(R.id.viewpager);
        viewpager.setPagingEnabled(true);
    }


    public static void slideFromRight(View view)
    {
        view.setTranslationX(view.getWidth());
        view.animate().translationX(0).setDuration(200);
    }

    public static void slideFromLeft(View view)
    {
        view.setTranslationX(-view.getWidth());
        view.animate().translationX(0).setDuration(200);
    }

    public static void shiftFabUp(Activity activity)
    {
        FloatingActionButton fab = (FloatingActionButton) activity.findViewById(R.id.fab);
        fab.animate().translationY(-75).setDuration(200);
    }
    public static void resetFab(Activity activity)
    {
        FloatingActionButton fab = (FloatingActionButton) activity.findViewById(R.id.fab);
        fab.animate().translationY(0).setDuration(200);
    }
}
