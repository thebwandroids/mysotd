package com.seii.theandroids.myshaveoftheday.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.seii.theandroids.myshaveoftheday.R;
import com.seii.theandroids.myshaveoftheday.model.ItemCategory;

import java.util.Collections;
import java.util.List;

/**
 *
 */
public class CategoryAdapter extends ArrayAdapter<ItemCategory>
{
    private List<ItemCategory> categories;

    public CategoryAdapter(Context context, int resource, List<ItemCategory> items) {
        super(context, resource, items);
        categories = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View view = convertView;

        if (view == null)
        {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            view = vi.inflate(R.layout.shave_den_list_row_layout, null);
        }

        ItemCategory category = getItem(position);

        if (category != null)
        {
            TextView title = (TextView)view.findViewById(R.id.itemText);
            title.setText(category.getTitle());
        }

        return view;
    }


    public ItemCategory getCategoryAt(int position)
    {
        return categories.get(position);
    }
}
