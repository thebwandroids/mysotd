package com.seii.theandroids.myshaveoftheday;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Clay.
 * This class handles the dialog fragment for merging categories within the application
 */
public class MergeFragment extends DialogFragment
{

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_merge_categories, container, false);

        return view;
    }
}