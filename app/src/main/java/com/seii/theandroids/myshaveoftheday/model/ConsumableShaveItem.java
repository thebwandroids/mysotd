package com.seii.theandroids.myshaveoftheday.model;

import com.seii.theandroids.myshaveoftheday.R;

import java.util.Calendar;

/**
 * Created by rajohnbutler on 2/10/16.
 */
public class ConsumableShaveItem extends RegularShaveItem {
    private int numberOfUses;

    public ConsumableShaveItem()
    {
        setLastUse(Calendar.getInstance());
        setPurchaseDate(Calendar.getInstance());
        setImage("");
    }

    @Override
    public int getMenu()
    {
        return R.array.onLongPressConsumable;
    }
    @Override
    public ITEM_TYPE getType()
    {
        return ITEM_TYPE.CONSUMABLE;
    }

    public int getNumberOfUses()
    {
        return numberOfUses;
    }

    @Override
    public void useItem()
    {
        numberOfUses++;
    }

    public void setNumberOfUses(int numberOfUses)
    {
        this.numberOfUses = numberOfUses;
    }
}
