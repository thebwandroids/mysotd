package com.seii.theandroids.myshaveoftheday.model;

import com.seii.theandroids.myshaveoftheday.R;

import java.util.Calendar;

/**
 * This class is responsible for managing the units of disposable shave items.
 * It is a child of ShaveItem.
 * Created by rajohnbutler on 2/10/16.
 */
public class DisposableShaveItem extends RegularShaveItem
{

    private int unitsLeft;

    /**
     * empty constructor
     */
    public DisposableShaveItem()
    {
        setLastUse(Calendar.getInstance());
        setPurchaseDate(Calendar.getInstance());
        setImage("");
    }


    public int getUnitsLeft(){
        return unitsLeft;
    }

    public void setUnitsLeft(int unitsLeft)
    {
        this.unitsLeft = unitsLeft;
    }

    @Override
    public void useItem(){
        unitsLeft--;
    }

    @Override
    public int getMenu()
    {
        return R.array.onLongPressDisposable;
    }
    @Override
    public ITEM_TYPE getType()
    {
        return ITEM_TYPE.DISPOSABLE;
    }
}



