package com.seii.theandroids.myshaveoftheday;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.seii.theandroids.myshaveoftheday.listener.OnBackPressedListener;
import com.seii.theandroids.myshaveoftheday.model.ConsumableShaveItem;
import com.seii.theandroids.myshaveoftheday.model.DisposableShaveItem;
import com.seii.theandroids.myshaveoftheday.model.ITEM_TYPE;
import com.seii.theandroids.myshaveoftheday.model.ItemCategory;
import com.seii.theandroids.myshaveoftheday.model.RegularShaveItem;
import com.seii.theandroids.myshaveoftheday.model.ShaveItem;
import com.seii.theandroids.myshaveoftheday.util.animationUtil;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Matt on 3/22/2016.
 */
public class AddEditItemFragment extends android.support.v4.app.Fragment {

    private EditText etDate;
    private EditText specialEditText;
    private EditText purchasePrice;
    private Calendar c;
    private DatePickerDialog.OnDateSetListener date;
    static final String DIRECTORY_SHAVE_DEN = "Shave Den";
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int PICK_IMAGE = 2;
    final private int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;
    final private int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE1 = 2;
    private static final int ADD_ITEM = 1;
    private static final int EDIT_ITEM = 2;
    private String mCurrentPhotoPath = "";
    private ImageView iv;
    private DBHandler db;
    private EditText name;
    private EditText note;
    private Spinner Itemtype;
    private int purchase_price;
    private int func;
    private String prevImagePath;
    private ShaveItem item, previtem;
    private ItemCategory category;

    public AddEditItemFragment()
    {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.add_entry_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu)
    {


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_done)
        {

            // adds new item if in add mode
            if (func == ADD_ITEM && Itemtype.getSelectedItem().toString().equals("Regular")) {

                ShaveItem newItem = new RegularShaveItem(name.getText().toString(), mCurrentPhotoPath);
                newItem.setNote(note.getText().toString());
                if(!purchasePrice.getText().toString().isEmpty())
                    newItem.setPurchasePrice(Double.parseDouble(purchasePrice.getText().toString()));
                else
                    newItem.setPurchasePrice(0);
                newItem.setLastUse(c);
                db.addShaveItem(newItem, category);

            }

            if (func == ADD_ITEM && Itemtype.getSelectedItem().toString().equals("Disposable")) {

                ShaveItem newItem = new DisposableShaveItem();
                newItem.setImage(mCurrentPhotoPath);
                if(!specialEditText.getText().toString().isEmpty())
                    ((DisposableShaveItem)newItem).setUnitsLeft(Integer.parseInt(specialEditText.getText().toString()));
                else
                    ((DisposableShaveItem)newItem).setUnitsLeft(0);
                newItem.setNote(note.getText().toString());
                if(!purchasePrice.getText().toString().isEmpty())
                    newItem.setPurchasePrice(Double.parseDouble(purchasePrice.getText().toString()));
                else
                    newItem.setPurchasePrice(0);
                newItem.setTitle(name.getText().toString());
                newItem.setLastUse(c);
                db.addShaveItem(newItem, category);

            }

            if (func == ADD_ITEM && Itemtype.getSelectedItem().toString().equals("Consumable")) {

                ShaveItem newItem = new ConsumableShaveItem();
                if(!specialEditText.getText().toString().isEmpty())
                    ((ConsumableShaveItem)newItem).setNumberOfUses(Integer.parseInt(specialEditText.getText().toString()));
                else
                    ((ConsumableShaveItem)newItem).setNumberOfUses(0);
                newItem.setImage(mCurrentPhotoPath);
                newItem.setNote(note.getText().toString());
                if(!purchasePrice.getText().toString().isEmpty())
                    newItem.setPurchasePrice(Double.parseDouble(purchasePrice.getText().toString()));
                else
                    newItem.setPurchasePrice(0);
                newItem.setTitle(name.getText().toString());
                newItem.setLastUse(c);
                db.addShaveItem(newItem, category);

            }
            // edits existing item if in edit mode
            else if (func == EDIT_ITEM)
           {
               String type = Itemtype.getSelectedItem().toString();
               ShaveItem newItem = null;
               if(type.equalsIgnoreCase("Regular"))
                   newItem = new RegularShaveItem();
               else if(type.equalsIgnoreCase("Consumable"))
               {
                   newItem = new ConsumableShaveItem();
                   if(!specialEditText.getText().toString().isEmpty())
                       ((ConsumableShaveItem)newItem).setNumberOfUses(Integer.parseInt(specialEditText.getText().toString()));
                   else
                       ((ConsumableShaveItem)newItem).setNumberOfUses(0);
               }
               else if(type.equalsIgnoreCase("Disposable"))
               {
                   newItem = new DisposableShaveItem();
                   if(!specialEditText.getText().toString().isEmpty())
                       ((DisposableShaveItem)newItem).setUnitsLeft(Integer.parseInt(specialEditText.getText().toString()));
                   else
                       ((DisposableShaveItem)newItem).setUnitsLeft(0);
               }
               newItem.setImage(mCurrentPhotoPath);
               newItem.setNote(note.getText().toString());
               if(!purchasePrice.getText().toString().isEmpty())
                   newItem.setPurchasePrice(Double.parseDouble(purchasePrice.getText().toString()));
               else
                   newItem.setPurchasePrice(0);
               newItem.setTitle(name.getText().toString());
               newItem.setLastUse(c);
               db.editShaveItem(previtem, newItem);
           }

            // Shows FAB and tabs and changes menu
            animationUtil.showTabs(getActivity());
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(category.getTitle());
            final MainActivity mainActivity = (MainActivity)getActivity();
            if (mainActivity.fab.getVisibility() == View.INVISIBLE)
                mainActivity.fab.setVisibility(View.VISIBLE);
            mainActivity.fab.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    animationUtil.hideTabs(mainActivity);
                    AddEditItemFragment newFragment = new AddEditItemFragment();
                    newFragment.setCategory(category);
                    FragmentTransaction transaction = mainActivity.getSupportFragmentManager().beginTransaction();
                    mainActivity.getSupportActionBar().setTitle("Add Item");
                    mainActivity.fab.setVisibility(View.INVISIBLE);
                    Bundle args = new Bundle();
                    args.putInt("FUNCTION", ADD_ITEM);
                    newFragment.setArguments(args);
                    transaction.replace(R.id.shave_den_root_frame, newFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            });

            // Goes to previous fragment
            getFragmentManager().popBackStack();
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_edit_item, container, false);

        func = getArguments().getInt("FUNCTION");
        name = (EditText) view.findViewById(R.id.name_edit_text);
        note = (EditText) view.findViewById(R.id.note_edit_text);
        Itemtype = (Spinner) view.findViewById(R.id.item_type_spinner);
        specialEditText = (EditText) view.findViewById(R.id.specialEditText);
        purchasePrice = (EditText)view.findViewById(R.id.purchase_price_edit_text);


        db = new DBHandler(getActivity(),null,null,1);

        // sets fields to current if in edit mode
        if (func == EDIT_ITEM)
        {
            previtem = item;
            name.setText(previtem.getTitle());
            note.setText(previtem.getNote());
            if(previtem.getType() == ITEM_TYPE.CONSUMABLE)
            {
                Itemtype.setSelection(2);
                specialEditText.setText(((ConsumableShaveItem) previtem).getNumberOfUses() + "");
            }
            else if(previtem.getType() == ITEM_TYPE.REGULAR)
                Itemtype.setSelection(0);
            else if(previtem.getType() == ITEM_TYPE.DISPOSABLE)
            {
                Itemtype.setSelection(1);
                specialEditText.setText(((DisposableShaveItem)previtem).getUnitsLeft() + "");
            }
        }

        if(Itemtype.getSelectedItemPosition() == 0) // Regular
        {
            specialEditText.setVisibility(View.GONE);
        }
        else if(Itemtype.getSelectedItemPosition() == 1) // Disposable
        {
            specialEditText.setHint("Number of Units");
            specialEditText.setVisibility(View.VISIBLE);
        }
        else if(Itemtype.getSelectedItemPosition() == 2) // Consumable
        {
            specialEditText.setHint("Number of times used");
            specialEditText.setVisibility(View.VISIBLE);
        }

        Itemtype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if(position == 0) // Regular
                {
                    specialEditText.setVisibility(View.GONE);
                }
                else if(position == 1) // Disposable
                {
                    specialEditText.setHint("Number of Units");
                    specialEditText.setVisibility(View.VISIBLE);
                }
                else if(position == 2) // Consumable
                {
                    specialEditText.setHint("Number of times used");
                    specialEditText.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });


        setDate(view);
        changePhoto(view);

        return view;
    }


    // Adds OnClickListener to add image button and displays the 'Change photo' dialog

    private void changePhoto(View v) {
        // Displays a dialog when camera button is clicked
        ImageView cameraButton = (ImageView) v.findViewById(R.id.add_image);
        iv = (ImageView) v.findViewById(R.id.imagePreview);

        //sets preview to current image if in edit mode
        if (func == EDIT_ITEM)
        {
            prevImagePath = item.getImage();
            mCurrentPhotoPath = prevImagePath;
            setPhoto();
        }

        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setTitle(R.string.change_photo);
                builder1.setCancelable(true);

                //Creates options
                builder1.setItems(R.array.photoOptions, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0)
                            takePhoto();
                        else
                            choosePhoto();
                    }
                });

                //Creates cancel button
                builder1.setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                builder1.show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result from image browser
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data == null)
                return;

            try {
                // Retrieves image from data in form of bitmap
                InputStream inputStream = getContext().getContentResolver().openInputStream(data.getData());
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
                Bitmap bitmap = BitmapFactory.decodeStream(bufferedInputStream);

                // Creates file path for image to be saved to
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
                String imageFileName = "JPEG_" + timeStamp;
                File storageDir = getActivity().getExternalFilesDir(DIRECTORY_SHAVE_DEN);
                mCurrentPhotoPath = storageDir.getPath() + "/" + imageFileName;

                // Saves bitmap as JPEG
                FileOutputStream out = null;
                try {
                    out = new FileOutputStream(mCurrentPhotoPath);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                setPhoto();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        // Result from camera
        else if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if (resultCode == Activity.RESULT_OK) {
                setPhoto();
            }
        }
    }

    // Sets date field to the current date and displays date picker dialog when user clicks the field
    private void setDate(View v)
    {
        //sets date field to current date
        etDate = (EditText) v.findViewById(R.id.purchase_date_edit_text);
        c = Calendar.getInstance();

        //sets date field to current if in edit mode
        if (func == EDIT_ITEM)
        {
            c = item.getPurchaseDate();
        }

        String cStr = new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(c.getTime());
        etDate.setText(cStr);

        // Creates date picker dialog
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                c.set(Calendar.YEAR, year);
                c.set(Calendar.MONTH, monthOfYear);
                c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
                etDate.setText(sdf.format(c.getTime()));
            }
        };

        // Sets listener on EditText to show date picker
        etDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(getContext(), date,
                        c.get(Calendar.YEAR), c.get(Calendar.MONTH),
                        c.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }


    // Opens the camera activity
    private void takePhoto()
    {
        // Checks if it has WRITE_EXTERNAL_STORAGE permission
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Displays explanation if it has one
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Explanation here

            } else {

                // Requests permission. onRequestPermissionsResult method handles user's input
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            }
        }

        else {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                // Create the File where the photo should go
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    // Error occurred while creating the File
                    System.out.println("Error creating image file");
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            Uri.fromFile(photoFile));

                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        }
    }


    // Creates path, name, and file where image will be stored
    private File createImageFile() throws IOException {
        File image = null;
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
            //System.out.println(timeStamp);
            String imageFileName = "JPEG_" + timeStamp;
            //System.out.println(imageFileName);
            //File storageDir = Environment.getExternalStorageDirectory();
            File storageDir = getActivity().getExternalFilesDir(DIRECTORY_SHAVE_DEN);
            //System.out.println(storageDir.getPath());
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
            //System.out.println(image.getPath() + " image path");

            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPath = image.getAbsolutePath();
        } catch(IOException e) {
            e.printStackTrace();
        }
        return image;
    }


    // Opens browser for user to select photo from storage
    private void choosePhoto()
    {
        // Checks if it has WRITE_EXTERNAL_STORAGE permission
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Displays explanation if it has one
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Explanation here
            } else {
                // Requests permission. onRequestPermissionsResult method handles user's input
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE1);
            }
        }
        else
        {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
        }
    }


    /**
     * Generates a bitmap from file and sets it in the preview ImageView
     */
    private void setPhoto()
    {
        System.out.println("Path" + mCurrentPhotoPath);
        if (!mCurrentPhotoPath.equals("") && new File(mCurrentPhotoPath).exists()) {
            // Get the dimensions of the View
            int targetW = iv.getWidth();
            int targetH = iv.getHeight();

            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;

            // Determine how much to scale down the image
            //int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            //bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;

            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
            iv.setImageBitmap(bitmap);
        }
    }


    private void setPhoto(BufferedInputStream b)
    {
        if (!mCurrentPhotoPath.equals("") && new File(mCurrentPhotoPath).exists()) {
            // Get the dimensions of the View
            int targetW = iv.getWidth();
            int targetH = iv.getHeight();

            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(b);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;

            // Determine how much to scale down the image
            int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;

            Bitmap bitmap = BitmapFactory.decodeStream(b);
            iv.setImageBitmap(bitmap);
        }
    }

    // Returns true if device is running Android 6.0 or higher, relevant for permission requests
    private boolean marshmallow() {
        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    takePhoto();

                } else {
                    // Permission denied
                    Toast.makeText(getActivity(), "Permission: WRITE_EXTERNAL_STORAGE required to add photo",
                            Toast.LENGTH_LONG).show();
                }
                return;
            }
            case PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    choosePhoto();

                } else {
                    // Permission denied
                    Toast.makeText(getActivity(), "Permission: WRITE_EXTERNAL_STORAGE required to add photo",
                            Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    public void setShaveItem(ShaveItem item)
    {
        this.item = item;
    }

    public void setCategory(ItemCategory category)
    {
        this.category = category;
    }

}
