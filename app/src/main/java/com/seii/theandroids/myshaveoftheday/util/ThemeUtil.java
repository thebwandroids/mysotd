package com.seii.theandroids.myshaveoftheday.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.seii.theandroids.myshaveoftheday.R;

/**
 *
 */
public class ThemeUtil
{
    public static void setTheme(Context context, THEME theme) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putInt(context.getString(R.string.prefs_theme_key), theme.getValue()).apply();
    }
    public static THEME getTheme(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return THEME.getThemeFromValue(Integer.parseInt(prefs.getString(context.getString(R.string.prefs_theme_key), "0")));
    }

    public enum THEME
    {
        BLUE(0),
        GREEN(1),
        RED(2),
        BLACK(3),
        CYAN(4),
        ORANGE(5),
        YELLOW(6),
        GREY(7),
        PURPLE(8);

        private int value;

        THEME(int value)
        {
            this.value = value;
        }

        public int getValue()
        {
            return this.value;
        }

        public static THEME getThemeFromValue(int value)
        {
            THEME theme = null;
            switch (value)
            {
                case(0):
                    theme = BLUE;
                break;
                case(1):
                    theme = GREEN;
                break;
                case(2):
                    theme = RED;
                break;
                case(3):
                    theme = BLACK;
                    break;
                case(4):
                    theme = CYAN;
                    break;
                case(5):
                    theme = ORANGE;
                    break;
                case(6):
                    theme = YELLOW;
                    break;
                case(7):
                    theme = GREY;
                    break;
                case(8):
                    theme = PURPLE;
                    break;
            }
            return theme;
        }
    }
}
