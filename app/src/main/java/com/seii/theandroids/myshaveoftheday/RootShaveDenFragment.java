package com.seii.theandroids.myshaveoftheday;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by Matt on 2/26/2016.
 */
public class RootShaveDenFragment extends Fragment
{

    private ShaveDenFragment mShaveDenFragment;

    public RootShaveDenFragment()
    {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mShaveDenFragment = new ShaveDenFragment();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu)
    {
        if(menu != null)
        {
            MenuItem item = menu.findItem(R.id.action_settings);
            if (item != null)
                item.setVisible(true);
            item = menu.findItem(R.id.action_search);
            if (item != null)
                item.setVisible(false);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
		/* Inflate the layout for this fragment */
        View view = inflater.inflate(R.layout.fragment_root_shave_den, container, false);

        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();

		/*
		 * When this container fragment is created, we fill it with our first
		 * "real" fragment
		 */
        transaction.replace(R.id.shave_den_root_frame, mShaveDenFragment);


        transaction.commit();

        return view;
    }

    public void setFloatingActionButtonAddCategory()
    {
        mShaveDenFragment.setFloatingActionButtonAddCategory();
    }


}

