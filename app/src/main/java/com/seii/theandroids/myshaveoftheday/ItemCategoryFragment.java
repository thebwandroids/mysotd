package com.seii.theandroids.myshaveoftheday;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.seii.theandroids.myshaveoftheday.adapter.CategoryAdapter;
import com.seii.theandroids.myshaveoftheday.adapter.LogAdapter;
import com.seii.theandroids.myshaveoftheday.adapter.ShaveItemAdapter;
import com.seii.theandroids.myshaveoftheday.listener.BaseBackPressedListener;
import com.seii.theandroids.myshaveoftheday.listener.OnBackPressedListener;
import com.seii.theandroids.myshaveoftheday.model.ConsumableShaveItem;
import com.seii.theandroids.myshaveoftheday.model.DisposableShaveItem;
import com.seii.theandroids.myshaveoftheday.model.ITEM_TYPE;
import com.seii.theandroids.myshaveoftheday.model.ItemCategory;
import com.seii.theandroids.myshaveoftheday.model.LogEntry;
import com.seii.theandroids.myshaveoftheday.model.ShaveItem;
import com.seii.theandroids.myshaveoftheday.util.animationUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * This fragment is responsible for displaying the items of a selected category.
 */
public class ItemCategoryFragment extends Fragment
{
    private List<ShaveItem> shaveItems;
    private ItemCategory category;

    public ItemCategoryFragment()
    {
        shaveItems = new ArrayList<ShaveItem>();
    }

    private DBHandler db;
    private ListView lv;

    private Activity mainActivity;
    private View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_item_category, container, false);
        setHasOptionsMenu(true);

        mainActivity = getActivity();
        db = new DBHandler(mainActivity,null,null,1);

        List<ItemCategory> categories = db.getCategories();
        for(ItemCategory tempCategory : categories)
        {
            if (tempCategory.getTitle().equals(category.getTitle()))
                shaveItems = tempCategory.getShaveItems();
        }

        // Sets up the listview for items by setting the content, as well as onClick and onLongClick
        populateItemListView();

        // Changes the top-level UI components
        setMainViews();


        // Changes the back button to go to the main Shave Den View (ShaveDenFragment)
//        OnBackPressedListener listener = new OnBackPressedListener()
//        {
//            @Override
//            public void doBack()
//            {
//                animationUtil.showTabs(mainActivity);
//                View viewpager =mainActivity.findViewById(R.id.viewpager);
//                animationUtil.slideFromLeft(viewpager);
//                ((AppCompatActivity)mainActivity).getSupportActionBar().setTitle(R.string.Shave_Den);
//
//                MainActivity mActivity = (MainActivity)mainActivity;
//                if (mActivity.fab.getVisibility() == View.INVISIBLE)
//                    mActivity.fab.setVisibility(View.VISIBLE);
//                mActivity.setOnBackPressedListener(null);
//            }
//        };
//        ((MainActivity) mainActivity).setOnBackPressedListener(listener);

        return view;
    }


    /**
     * Modifies the UI components of the main Activity, such as the Toolbar title, Floating Action Button, and tabs.
     * For this view, the tabs are hidden, the title is set to the current displayed category name, and the FAB is set to add items.
     */
    private void setMainViews()
    {
        // Sets the title to the current category
        ((AppCompatActivity) mainActivity).getSupportActionBar().setTitle(category.getTitle());

        final MainActivity mActivity = (MainActivity)mainActivity;
        animationUtil.hideTabs(mActivity);
        if (mActivity.fab.getVisibility() == View.INVISIBLE)
            mActivity.fab.setVisibility(View.VISIBLE);
        (mActivity).fab.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                AddEditItemFragment newFragment = new AddEditItemFragment();
                newFragment.setCategory(category);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                (mActivity).getSupportActionBar().setTitle("Add Item");
                (mActivity).fab.setVisibility(View.INVISIBLE);
                Bundle args = new Bundle();
                args.putInt("FUNCTION", 1);
                newFragment.setArguments(args);
                transaction.replace(R.id.shave_den_root_frame, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });
    }


    /**
     * Uses the global variables, defined in the onCreateView, to set the item list view's adapter and click listeners.
     */
    private void populateItemListView()
    {
        // Populates ListView with the shave items from the category it received
        lv = (ListView) view.findViewById(R.id.categoryListView);
        lv.setAdapter(new ShaveItemAdapter(mainActivity, 0, this.shaveItems));

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                MainActivity mActivity = (MainActivity) mainActivity;
                mActivity.fab.setVisibility(View.INVISIBLE);

                AddEditItemFragment newFragment = new AddEditItemFragment();
                newFragment.setCategory(category);
                newFragment.setShaveItem(shaveItems.get(position));
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                Bundle args = new Bundle();
                args.putInt("FUNCTION", 2);
                newFragment.setArguments(args);

                ((AppCompatActivity) mainActivity).getSupportActionBar().setTitle(shaveItems.get(position).getTitle());


                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                transaction.replace(R.id.shave_den_root_frame, newFragment);

                transaction.addToBackStack(null);

                // Commit the transaction
                transaction.commit();
            }
        });

        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           final int pos, long id)
            {
                final int position = pos;
                AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);

                int menu = R.array.onLongPressShaveItem;

                if (shaveItems.get(pos).getType() == ITEM_TYPE.CONSUMABLE)
                    menu = R.array.onLongPressConsumable;
                else if (shaveItems.get(pos).getType() == ITEM_TYPE.DISPOSABLE)
                    menu = R.array.onLongPressDisposable;

                final int m = menu;

                builder.setTitle(shaveItems.get(pos).getTitle())
                        .setItems(menu, new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int which)
                            {
                                final ShaveItem item = shaveItems.get(position);
                                if (which == 0) // Edit
                                {
                                    MainActivity mActivity = (MainActivity)mainActivity;
                                    mActivity.fab.setVisibility(View.INVISIBLE);

                                    AddEditItemFragment newFragment = new AddEditItemFragment();
                                    newFragment.setCategory(category);
                                    newFragment.setShaveItem(item);
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                    Bundle args = new Bundle();
                                    args.putInt("FUNCTION", 2);
                                    newFragment.setArguments(args);

                                    ((AppCompatActivity) mainActivity).getSupportActionBar().setTitle(item.getTitle());


                                    // Replace whatever is in the fragment_container view with this fragment,
                                    // and add the transaction to the back stack
                                    transaction.replace(R.id.shave_den_root_frame, newFragment);

                                    transaction.addToBackStack(null);

                                    // Commit the transaction
                                    transaction.commit();
                                }
                                else if (which == 1) // Delete
                                {
                                    animationUtil.shiftFabUp(mainActivity);
                                    final CoordinatorLayout coordinatorLayout = (CoordinatorLayout)view.findViewById(R.id.item_category_layout);
                                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Item Deleted", Snackbar.LENGTH_LONG)
                                            .setAction("UNDO", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v)
                                                {
                                                    Snackbar snackbar1 = Snackbar.make(coordinatorLayout, "Item Restored",Snackbar.LENGTH_SHORT);
                                                    snackbar1.show();

                                                    shaveItems.add(position,item);
                                                    lv.setAdapter(new ShaveItemAdapter(mainActivity, 0, shaveItems));
                                                    animationUtil.resetFab(mainActivity);
                                                }
                                            })
                                            .setCallback(new Snackbar.Callback()
                                            {
                                                @Override
                                                public void onDismissed(Snackbar snackbar, int event)
                                                {
                                                    animationUtil.resetFab(mainActivity);
                                                    if(event == Snackbar.Callback.DISMISS_EVENT_TIMEOUT)
                                                        db.deleteShaveItem(item.getTitle());
                                                }
                                            });
                                    snackbar.show();
                                    shaveItems.remove(position);
                                }
                                else if (which == 2) // Update date
                                {
                                    item.setLastUse(Calendar.getInstance());
                                    shaveItems.set(position, item);
                                    db.editShaveItem(item, item);

                                }
                                else if (which == 3) // increase uses
                                {
                                    if (m == R.array.onLongPressConsumable) // Increase uses (consumable)
                                    {
                                        item.useItem();
                                    } else // Decrease units (disposable)
                                    {
                                        item.useItem();
                                    }
                                    shaveItems.set(position, item);
                                    db.editShaveItem(item, item);
                                }

                                lv.setAdapter(new ShaveItemAdapter(mainActivity, 0, shaveItems));
                            }
                        });
                Dialog d = builder.create();
                d.show();

                return true;
            }
        });
    }


    // Setters for giving this fragment the necessary data to operate
    public void setShaveItems(List<ShaveItem> shaveItems)
    {
        this.shaveItems = shaveItems;
    }
    public void setCategory(ItemCategory category)
    {
        this.category = category;
    }
}
