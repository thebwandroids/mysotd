package com.seii.theandroids.myshaveoftheday;

import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ExpandableListView;

import com.seii.theandroids.myshaveoftheday.adapter.ItemExpandableListAdapter;
import com.seii.theandroids.myshaveoftheday.model.ItemCategory;
import com.seii.theandroids.myshaveoftheday.model.ShaveItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * AddItemToEntryFragment is responsible for populating an ExpandableListView with all of the Categories and their ShaveItems.
 * Each ShaveItem has a checkbox that can be checked off to automatically add that item to the AddEntryFragment's description field.
 */
public class AddItemToEntryFragment extends DialogFragment
{

    private ItemExpandableListAdapter adapter;
    private List<ShaveItem> items;

    public AddItemToEntryFragment()
    {

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_item_to_entry, container, false);
        setHasOptionsMenu(true);

        // Gets List of Categories and HashMap of ShaveItems from database
        items = new ArrayList<>();
        ExpandableListView elv = (ExpandableListView) view.findViewById(R.id.expandableListView);
        DBHandler db = new DBHandler(getActivity(),null,null,1);
        final List<ItemCategory> categories = db.getCategories();
        HashMap<ItemCategory, List<ShaveItem>> mapItems = new HashMap<>();
        for (int i = 0; i < categories.size(); i++)
        {
            mapItems.put(categories.get(i), categories.get(i).getShaveItems());
        }

        // Sets ExpandableListView adapter
        adapter = new ItemExpandableListAdapter(getActivity(), categories, mapItems);
        elv.setAdapter(adapter);

        // Sets OnChildCLickListener for when ShaveItems are clicked. Toggles checkbox and adds item to a List
        elv.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                ShaveItem item = adapter.getChild(groupPosition, childPosition);
                CheckBox checkBox = (CheckBox) v.findViewById(R.id.checkBox);
                if (checkBox.isChecked()) {
                    checkBox.setChecked(false);
                    items.remove(item);

                } else {
                    checkBox.setChecked(true);
                    item = adapter.getChild(groupPosition, childPosition);
                    items.add(item);
                }

                return true;
            }
        });

        return view;
    }


    public List<ShaveItem> getItems()
    {
        return items;
    }

}



