package com.seii.theandroids.myshaveoftheday.model;

import android.util.Log;

import com.seii.theandroids.myshaveoftheday.model.LogEntry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.*;

/**
 * Created by Russell on 4/22/2016.
 */
public class LogEntryTest {

    LogEntry entry;
    LogEntry entry2;
    LogEntry entry3;

    @Before
    public void setUp() throws Exception {
        entry = new LogEntry();

    }

    @Test
    public void testLogEntry() throws Exception {
        entry2 = new LogEntry("desc", "img");
        Calendar c = Calendar.getInstance();
        entry3 = new LogEntry("desc", "comment", c, "img");
        assertEquals("desc", entry2.getDescription());
        assertEquals("img", entry2.getImage());
        assertEquals("comment", entry3.getComment());
        assertEquals(c, entry3.getDate());
    }

    @Test
    public void testGetDate() throws Exception {
        Calendar c = Calendar.getInstance();
        entry.setDate(c);
        assertEquals(c, entry.getDate());
    }

    @Test
    public void testSetDate() throws Exception {

    }

    @Test
    public void testShare() throws Exception {

    }

    @Test
    public void testCompareTo() throws Exception {
        Calendar c = Calendar.getInstance();
        c.set(1990,10,11);
        entry.setDate(c);

        Calendar otherC = Calendar.getInstance();
        LogEntry otherEntry = new LogEntry();
        otherC.set(1990, 10, 10);
        otherEntry.setDate(otherC);

        assertTrue(entry.compareTo(otherEntry) > 0);

        otherC.set(1990,9,10);

        assertTrue(entry.compareTo(otherEntry) > 0);

    }

    @Test
    public void testGetDescription() throws Exception {
        entry.setDescription("desc");
        assertEquals("desc", entry.getDescription());


    }

    @Test
    public void testGetComment() throws Exception {
        entry.setComment("comment");
        assertEquals("comment", entry.getComment());


    }

    @Test
    public void testGetImage() throws Exception {
        entry.setImage("image_path");
        assertEquals("image_path", entry.getImage());

    }

    @Test
    public void testSetDescription() throws Exception {

    }

    @Test
    public void testSetComment() throws Exception {

    }

    @Test
    public void testSetImage() throws Exception {

    }
}