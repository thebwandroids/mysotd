package com.seii.theandroids.myshaveoftheday.model;

import com.seii.theandroids.myshaveoftheday.model.ItemCategory;
import com.seii.theandroids.myshaveoftheday.model.RegularShaveItem;
import com.seii.theandroids.myshaveoftheday.model.ShaveItem;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Russell on 4/22/2016.
 */
public class ItemCategoryTest {

    ItemCategory cat;

    @Before
    public void setUp() throws Exception {
        cat = new ItemCategory();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testGetTitle() throws Exception {
        cat.setTitle("title");
        assertEquals("title", cat.getTitle());
    }

    @Test
    public void testSetTitle() throws Exception {

    }

    @Test
    public void testAddItem() throws Exception {
        ShaveItem item = new RegularShaveItem("item1");
        cat.addItem(item);
        assertEquals("item1", cat.getShaveItems().get(0).getTitle());

    }

    @Test
    public void testGetShaveItems() throws Exception {
        ShaveItem item1 = new RegularShaveItem("item1");
        ShaveItem item2 = new RegularShaveItem("item2");
        cat.addItem(item1);
        cat.addItem(item2);
        assertEquals(2, cat.getShaveItems().size());
    }

    @Test
    public void testMergeWith() throws Exception {
        cat.setTitle("cat1");
        ItemCategory otherCat = new ItemCategory("cat2");
        cat.mergeWith(otherCat);
        assertEquals("cat1 and cat2", cat.getTitle());
    }

    @Test
    public void testDeleteItem() throws Exception {
        ShaveItem item1 = new RegularShaveItem("item1");
        ShaveItem item2 = new RegularShaveItem("item2");
        cat.addItem(item1);
        cat.addItem(item2);
        cat.deleteItem("item2");
        assertEquals(1, cat.getShaveItems().size());
    }
}