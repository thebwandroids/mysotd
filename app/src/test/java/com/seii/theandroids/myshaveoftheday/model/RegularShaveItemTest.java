package com.seii.theandroids.myshaveoftheday.model;

import com.seii.theandroids.myshaveoftheday.model.ITEM_TYPE;
import com.seii.theandroids.myshaveoftheday.model.RegularShaveItem;
import com.seii.theandroids.myshaveoftheday.model.ShaveItem;

import junit.framework.TestCase;

import java.util.Calendar;

/**
 * Created by Russell on 4/22/2016.
 */
public class RegularShaveItemTest extends TestCase {

    ShaveItem item;

    public void setUp() throws Exception {
        super.setUp();
        item = new RegularShaveItem();

    }

    public void testRegularShaveItem() {
        ShaveItem item2 = new RegularShaveItem("title", "image");
        assertEquals("title", item2.getTitle());
        assertEquals("image", item2.getImage());
    }

    public void testGetMenu() throws Exception {

    }

    public void testGetType() throws Exception {
        assertEquals(ITEM_TYPE.REGULAR, item.getType());
    }

    public void testGetTitle() throws Exception {
        item.setTitle("test");
        assertEquals("test", item.getTitle());
    }

    public void testGetPurchaseDate() throws Exception {
        Calendar c = Calendar.getInstance();
        item.setPurchaseDate(c);
        assertEquals(c, item.getPurchaseDate());

    }

    public void testGetPurchasePrice() throws Exception {
        item.setPurchasePrice(4.0);
        assertEquals(4.0, item.getPurchasePrice());

    }

    public void testGetLastUse() throws Exception {
        Calendar c = Calendar.getInstance();
        item.setLastUse(c);
        assertEquals(c, item.getLastUse());
    }

    public void testGetNote() throws Exception {
        item.setNote("a note");
        assertEquals("a note", item.getNote());

    }

    public void testGetImage() throws Exception {
        item.setImage("image_path");
        assertEquals("image_path", item.getImage());

    }

    public void testSetTitle() throws Exception {

    }

    public void testSetImage() throws Exception {

    }

    public void testSetPurchaseDate() throws Exception {

    }

    public void testSetPurchasePrice() throws Exception {

    }

    public void testSetLastUse() throws Exception {

    }

    public void testSetNote() throws Exception {

    }

    public void testUseItem() throws Exception {

    }
}