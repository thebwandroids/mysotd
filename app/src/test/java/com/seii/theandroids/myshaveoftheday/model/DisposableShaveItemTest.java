package com.seii.theandroids.myshaveoftheday.model;

import com.seii.theandroids.myshaveoftheday.model.DisposableShaveItem;
import com.seii.theandroids.myshaveoftheday.model.ITEM_TYPE;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Russell on 4/22/2016.
 */
public class DisposableShaveItemTest {

    DisposableShaveItem item;

    @Before
    public void setUp() throws Exception {
        item = new DisposableShaveItem();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testGetUnitsLeft() throws Exception {
        item.setUnitsLeft(50);
        assertEquals(50, item.getUnitsLeft());
    }

    @Test
    public void testSetUnitsLeft() throws Exception {

    }

    @Test
    public void testUseItem() throws Exception {
        item.setUnitsLeft(50);
        item.useItem();
        assertEquals(49, item.getUnitsLeft());
    }

    @Test
    public void testGetMenu() throws Exception {

    }

    @Test
    public void testGetType() throws Exception {
        assertEquals(ITEM_TYPE.DISPOSABLE, item.getType());
    }
}