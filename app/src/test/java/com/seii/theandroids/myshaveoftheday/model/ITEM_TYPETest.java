package com.seii.theandroids.myshaveoftheday.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Russell on 4/29/2016.
 */
public class ITEM_TYPETest {

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testGetValue() throws Exception {
        assertEquals(0, ITEM_TYPE.REGULAR.getValue());
    }

    @Test
    public void testGetTypeFromValue() throws Exception {
        assertEquals(ITEM_TYPE.REGULAR, ITEM_TYPE.getTypeFromValue(0));
        assertEquals(ITEM_TYPE.DISPOSABLE, ITEM_TYPE.getTypeFromValue(1));
        assertEquals(ITEM_TYPE.CONSUMABLE, ITEM_TYPE.getTypeFromValue(2));

    }
}