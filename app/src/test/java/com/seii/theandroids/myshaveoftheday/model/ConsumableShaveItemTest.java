package com.seii.theandroids.myshaveoftheday.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Clay on 4/29/2016.
 */
public class ConsumableShaveItemTest
{
    ConsumableShaveItem item;

    @Before
    public void setUp() throws Exception {
        item = new ConsumableShaveItem();
    }

    @Test
    public void testGetType() throws Exception
    {
        assertEquals(item.getType(), ITEM_TYPE.CONSUMABLE);
    }

    @Test
    public void testGetNumberOfUses() throws Exception
    {
        item.setNumberOfUses(50);
        assertEquals(item.getNumberOfUses(), 50);
    }

    @Test
    public void testUseItem() throws Exception
    {
        item.setNumberOfUses(50);
        item.useItem();
        assertEquals(item.getNumberOfUses(), 51);
    }
}